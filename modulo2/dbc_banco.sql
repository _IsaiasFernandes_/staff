CREATE TABLE BANCO_DADOS (
    ID_BANCO_DADOS INT PRIMARY KEY NOT NULL,
    CPF CHAR(11) UNIQUE NOT NULL,
    NOME VARCHAR2(255) DEFAULT 'USUARIO' NOT NULL,
    ANIVERSARIO DATE NULL
    --, CONSTRAINT PK_ID_BANCO_DADOS PRIMARY KEY (ID_BANCO_DADOS)
   
);

CREATE TABLE BANCO_DADOS_DOIS (
    ID_BANCO_DADOS_2 INT PRIMARY KEY NOT NULL,
    ID_BANCO_DADOS INT NOT NULL,
    NOME VARCHAR2(255) DEFAULT 'USUARIO' NOT NULL,
    
    CONSTRAINT FK_ID_BANCO_DADOS FOREIGN KEY (ID_BANCO_DADOS) REFERENCES BANCO_DADOS(ID_BANCO_DADOS)
);

ALTER TABLE BANCO_DADOS_DOIS
    ADD ENDERECO VARCHAR2(255) NOT NULL;
    
    
DROP TABLE BANCO_DADOS; 
    
    
    
    
    
    
    