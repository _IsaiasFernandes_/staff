/*CREATE SEQUENCE BANCO_DADOS_SEQ
    START WITH 1
    INCREMENT BY 1
    NOCYCLE; */

-- CREATE SEQUENCE BANCO_DADOS_DOIS_SEQ;

--C

INSERT INTO banco_dados(id_banco_dados, cpf, nome) values (BANCO_DADOS_SEQ.nextval, '00011122233', 'TESTE1');
INSERT INTO banco_dados(id_banco_dados, cpf, nome) values (BANCO_DADOS_SEQ.nextval, '01011122233', 'TESTE2');
INSERT INTO banco_dados(id_banco_dados, cpf, nome) values (BANCO_DADOS_SEQ.nextval, '02011122233', 'TESTE3');


--R

--SELECT * FROM banco_dados;
SELECT b.id_banco_dados as id, b.cpf as cpf1, b.nome FROM banco_dados B;
SELECT 1 FROM BANCO_DADOS;
SELECT COUNT(1) AS CONT  FROM BANCO_DADOS;

SELECT SUM(1) FROM BANCO_DADOS;

--U

UPDATE banco_dados
SET NOME = 'TESTE'
WHERE NOME = 'TESTE1'; -- <> < > = ISNULL INSNOTNULL IN IN NOT LIKE
--D

DELETE BANCO_DADOS
WHERE NOME = 'TESTE2';

