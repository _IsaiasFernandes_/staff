
public class DwarfBarbaLonga extends Dwarf {
    private Sorteador sorteador;
    
    public DwarfBarbaLonga( String nome ){
        super(nome);
        this.sorteador = new DadoD6();
    }
    
    public DwarfBarbaLonga( String nome, Sorteador sorteador ){
        super(nome);
        this.sorteador = sorteador;
    }
    
    public void sofrerDano() {
        boolean devePerderVida = sorteador.sortear() <= 4;
        if( devePerderVida ) {
            super.sofrerDano();
        }
    }
}