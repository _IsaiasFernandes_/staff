import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class InventarioTest
{    
    @Test
    public void adicionarUmItem() {
        Inventario inventario = new Inventario();
        Item item = new Item(1, "Espada");
        inventario.adicionar(item);
        assertEquals(item, inventario.obter(0));
    }
    
    @Test
    public void adicionarDoisItens() {
        Inventario inventario = new Inventario();
        Item item = new Item(1, "Espada");
        Item escudo = new Item(1, "Escudo");
        inventario.adicionar(item);
        inventario.adicionar(escudo);
        assertEquals(item, inventario.obter(0));
    }
    
    @Test
    public void adicionarUmItemEObter() {
        Inventario inventario = new Inventario();
        Item item = new Item(1, "Espada");
        inventario.adicionar(item);
        assertEquals(item, inventario.obter(0));
    }
    
    @Test
    public void adicionarUmItemERemover() {
        Inventario inventario = new Inventario();
        Item item = new Item(1, "Espada");
        inventario.adicionar(item);
        inventario.remover(0);
        assertNull(inventario.obter(0));
    }
    
    @Test
    public void getDescricoesVariosItens() {
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(2, "Escudo");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        String resultado = inventario.getDescricoes();
        assertEquals( "Espada,Escudo", resultado );
    }
    
    @Test
    public void getItemMaiorQuantidade() {
        Inventario inventario = new Inventario();
        Item lanca = new Item(1, "Lanca");
        Item espada = new Item(5, "Espada");
        Item escudo = new Item(2, "Escudo");
        inventario.adicionar(lanca);
        inventario.adicionar(escudo);
        inventario.adicionar(espada);
        Item resultado = inventario.getItemComMaiorQuantidade();
        assertEquals(espada, resultado);
    }
    
    @Test
    public void getItemMaiorQuantidadeComMesmaQuantidade() {
        Inventario inventario = new Inventario();
        Item lanca = new Item(5, "Lanca");
        Item espada = new Item(5, "Espada");
        Item escudo = new Item(1, "Escudo");
        inventario.adicionar(lanca);
        inventario.adicionar(escudo);
        inventario.adicionar(espada);
        Item resultado = inventario.getItemComMaiorQuantidade();
        assertEquals(lanca, resultado);
    }
    
    @Test
    public void buscarNoInventario() {
        Inventario inventario = new Inventario();
        Item lanca = new Item(5, "Lanca");
        Item espada = new Item(5, "Espada");
        Item escudo = new Item(1, "Escudo");
        inventario.adicionar(lanca);
        inventario.adicionar(escudo);
        inventario.adicionar(espada);
        Item resultado = inventario.buscar("Escudo");
        assertEquals(escudo, resultado);
    }
    
    @Test
    public void inventarioInvertido() {
        Inventario inventario = new Inventario();
        ArrayList<Item> itensInvertidos = new ArrayList<>();
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(2, "Escudo");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        itensInvertidos = inventario.inverter();
        assertEquals("Escudo", itensInvertidos.get(0).getDescricao());
    }
}
