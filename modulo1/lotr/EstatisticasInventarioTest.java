import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EstatisticasInventarioTest
{
    @Test
    public void calcularMedia() {
        Inventario inventario = new Inventario();
        Item lanca = new Item(5, "Lanca");
        Item espada = new Item(5, "Espada");
        Item escudo = new Item(1, "Escudo");
        Item arco = new Item(3, "Arco");
        inventario.adicionar(lanca);
        inventario.adicionar(escudo);
        inventario.adicionar(espada);
        inventario.adicionar(arco);
        EstatisticasInventario estatisticasInventario = new EstatisticasInventario(inventario);
        assertEquals(3 ,estatisticasInventario.calcularMedia(), 1e-8);
    }
    
    @Test
    public void calcularMediana() {
        Inventario inventario = new Inventario();
        Item lanca = new Item(5, "Lanca");
        Item espada = new Item(5, "Espada");
        Item escudo = new Item(1, "Escudo");
        Item arco = new Item(3, "Arco");
        inventario.adicionar(lanca);
        inventario.adicionar(escudo);
        inventario.adicionar(espada);
        inventario.adicionar(arco);
        EstatisticasInventario estatisticasInventario = new EstatisticasInventario(inventario);
        assertEquals(4 ,estatisticasInventario.calcularMediana(), 1e-8);
    }
    
    @Test
    public void qtdItensAcimaDaMedia() {
        Inventario inventario = new Inventario();
        Item lanca = new Item(5, "Lanca");
        Item espada = new Item(5, "Espada");
        Item escudo = new Item(1, "Escudo");
        Item arco = new Item(3, "Arco");
        inventario.adicionar(lanca);
        inventario.adicionar(escudo);
        inventario.adicionar(espada);
        inventario.adicionar(arco);
        EstatisticasInventario estatisticasInventario = new EstatisticasInventario(inventario);
        assertEquals(2 ,estatisticasInventario.qtdItensAcimaDaMedia() );
    }
}
