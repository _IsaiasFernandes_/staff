import java.util.*;

public class PaginadorInventario
{
  private Inventario inventario;
  private int marcadorInicial;
  
  public PaginadorInventario(Inventario inventario) {
      this.inventario = inventario;
  }
  
  public void pular(int marcadorInicial) {
      this.marcadorInicial = marcadorInicial > 0 ? marcadorInicial : 0;
  }
  
  public ArrayList<Item> limitar(int limitador) {
      ArrayList<Item> subConjunto = new ArrayList<>();
      int fim = this.marcadorInicial + limitador;
      for(int i = this.marcadorInicial; i < fim && i < this.inventario.getItens().size(); i ++) {
          subConjunto.add(this.inventario.obter(i));
      }
      
      return subConjunto;
  }
}
