import java.util.*;

public class ElfoVerde extends Elfo {
    private final ArrayList<String> DESCRICOES_VALIDAS = new ArrayList<>(
        Arrays.asList(
            "Espada de aço valiriano",
            "Arco de Vidro",
            "Flecha de Vidro"
        )
    );
    
    public ElfoVerde( String nome ) {
        super(nome);
        this.qtdExperienciaPorAtaque = 2;
    }
    
    private boolean isDescricaoValida(String descricao) {
        return DESCRICOES_VALIDAS.contains(descricao);
    }
    
    @Override
    public void ganharItem( Item item ){
        if( this.isDescricaoValida(item.getDescricao()) ) {
            this.inventario.adicionar(item);
        }
    }
    
    @Override
    public void perderItem( Item item ){
        if( this.isDescricaoValida(item.getDescricao() ) ) {
            this.inventario.remover(item);
        }
    }
}