import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoDaLuzTest {
    @Test
    public void elfoDaLuzAtirarFlechas(){
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        feanor.getInventario().obter(1).setQuantidade(1000);
        
        feanor.atirarFlecha(new Dwarf("Balin"));
        feanor.atirarFlecha(new Dwarf("Balin"));
        feanor.atirarFlecha(new Dwarf("Balin"));
        feanor.atirarFlecha(new Dwarf("Balin"));
        feanor.atirarFlecha(new Dwarf("Balin"));

        assertEquals(100, feanor.getVida(), 1e-9 );
    }
    
    @Test
    public void elfoDaLuzAtacarUmaVezComEspada(){
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        feanor.getInventario().obter(1).setQuantidade(1000);
        
        feanor.atacarComEspada(new Dwarf("Balin"));

        assertEquals( 79, feanor.getVida(), 1e-9 );
    }
    
    @Test
    public void elfoDaLuzAtacarDuasVezesComEspada(){
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        feanor.getInventario().obter(1).setQuantidade(1000);
        
        feanor.atacarComEspada(new Dwarf("Balin"));
        feanor.atacarComEspada(new Dwarf("Balin"));

        assertEquals( 89, feanor.getVida(), 1e-9 );
    }
    
    
    @Test
    public void elfoDaLuzAtacarOitoVezesComEspada(){
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        feanor.getInventario().obter(1).setQuantidade(1000);
        
        feanor.atacarComEspada(new Dwarf("Balin"));
        feanor.atacarComEspada(new Dwarf("Balin"));
        feanor.atacarComEspada(new Dwarf("Balin"));
        feanor.atacarComEspada(new Dwarf("Balin"));
        feanor.atacarComEspada(new Dwarf("Balin"));
        feanor.atacarComEspada(new Dwarf("Balin"));
        feanor.atacarComEspada(new Dwarf("Balin"));
        feanor.atacarComEspada(new Dwarf("Balin"));

        assertEquals( 56, feanor.getVida(), 1e-9 );
    }
}
