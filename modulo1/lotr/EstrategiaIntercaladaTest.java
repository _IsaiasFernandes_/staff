import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class EstrategiaIntercaladaTest
{
    @Test
    public void exercitoIntercaladoComecandoComElfoVerde() {
        EstrategiaIntercalada estrategia = new EstrategiaIntercalada();
        Elfo green1 = new ElfoVerde("Green 1");
        Elfo green2 = new ElfoVerde("Green 2");
        Elfo night1 = new ElfoNoturno("Night 1");
        Elfo night2 = new ElfoNoturno("Night 2");
        
        ArrayList<Elfo> elfosEntrada = new ArrayList<>(
            Arrays.asList(green1, green2, night1, night2)
        );
        
        ArrayList<Elfo> esperado = new ArrayList<>(
            Arrays.asList(green1, night1, green2, night2)
        );
        
        ArrayList<Elfo> obtidos = estrategia.getOrdemDeAtaque(elfosEntrada);
        
        assertEquals(esperado, obtidos);
    }
    
    @Test
    public void exercitoIntercaladoComecandoComElfoNoturno() {
        EstrategiaIntercalada estrategia = new EstrategiaIntercalada();
        Elfo green1 = new ElfoVerde("Green 1");
        Elfo green2 = new ElfoVerde("Green 2");
        Elfo night1 = new ElfoNoturno("Night 1");
        Elfo night2 = new ElfoNoturno("Night 2");
        
        ArrayList<Elfo> elfosEntrada = new ArrayList<>(
            Arrays.asList(night1, night2, green1, green2 )
        );
        
        ArrayList<Elfo> esperado = new ArrayList<>(
            Arrays.asList(night1, green1, night2, green2 )
        );
        
        ArrayList<Elfo> obtidos = estrategia.getOrdemDeAtaque(elfosEntrada);
        
        assertEquals(esperado, obtidos);
    }
}
