import java.util.*;

public class ElfoDaLuz extends Elfo {
    private int contadorDeAtaques = 0;
    private final double QTD_VIDA_GANHA = 10;
    private final ArrayList<String> DESCRICOES_OBRIGATORIAS = new ArrayList<>(
        Arrays.asList(
            "Espada de Galvorn"
        )
    );

    public ElfoDaLuz (String nome) {
        super(nome);
        this.qtdDano = 21;
        this.receberVida = QTD_VIDA_GANHA;
        this.inventario.adicionar(new ItemSempreExistente(1, DESCRICOES_OBRIGATORIAS.get(0)));
    }
    
    public Item getEspada() {
        return this.getInventario().buscar(DESCRICOES_OBRIGATORIAS.get(0));
    }
    
    private boolean devePerderVida() {
        return contadorDeAtaques % 2 == 1;
    }
    
    public void perderItem(Item item) {
        boolean possoPerder = !DESCRICOES_OBRIGATORIAS.contains(item.getDescricao());
        
        if(possoPerder ) {
            super.perderItem(item);
        }
    }
    
    public void atacarComEspada(Dwarf dwarf) {
        this.contadorDeAtaques ++;
        dwarf.sofrerDano();
        
        if(devePerderVida()){
                this.sofrerDano();
        } else {
                this.receberVida();
         }     
    }
}
