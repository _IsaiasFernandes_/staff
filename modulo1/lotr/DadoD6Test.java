import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DadoD6Test {
    
    @Test
    public void SortearNumero() {
        DadoD6 dado = new DadoD6();
        
        assertEquals(6, dado.sortear());
    }
}
