package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.AcessosEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AcessosRepository extends CrudRepository<AcessosEntity, Integer> {
    List<AcessosEntity> findAllBySaldoCliente(SaldoClienteEntity saldoCliente);
}
