package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ClientesEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;

import java.util.List;

public class ContatoDTO {

    private Integer id;
    private String valor;
    private ClientesEntity clientes;
    private TipoContatoEntity tipoContato;

    public ContatoDTO() {
    }

    public ContatoDTO(ContatoEntity contato) {
        this.id = contato.getId();
        this.valor = contato.getValor();
        this.clientes = contato.getCliente();
        this.tipoContato = contato.getTipoContato();
    }

    public ContatoEntity convert() {
        ContatoEntity contato = new ContatoEntity();
        contato.setId(this.id);
        contato.setValor(this.valor);
        contato.setCliente(this.clientes);
        contato.setTipoContato(this.tipoContato);

        return contato;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public ClientesEntity getClientes() {
        return clientes;
    }

    public void setClientes(ClientesEntity clientes) {
        this.clientes = clientes;
    }

    public TipoContatoEntity getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContatoEntity tipoContato) {
        this.tipoContato = tipoContato;
    }
}
