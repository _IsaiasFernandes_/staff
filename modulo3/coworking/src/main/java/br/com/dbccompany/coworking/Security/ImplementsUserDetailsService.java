package br.com.dbccompany.coworking.Security;

import br.com.dbccompany.coworking.Entity.UsuariosEntity;
import br.com.dbccompany.coworking.Repository.UsuariosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ImplementsUserDetailsService implements UserDetailsService {

    @Autowired
    private UsuariosRepository repository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<UsuariosEntity> usuario = repository.findByUsername(username);

        if (usuario.isPresent()) {
            return usuario.get();
        }

        throw new UsernameNotFoundException("Usuario não encontrado");
    }
}
