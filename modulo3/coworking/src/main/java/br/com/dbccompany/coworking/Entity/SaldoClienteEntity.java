package br.com.dbccompany.coworking.Entity;

import br.com.dbccompany.coworking.Entity.Enum.TipoContratacao;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = SaldoClienteEntity.class)
public class SaldoClienteEntity extends EntityAbstract<SaldoClienteEntityId>{

    @EmbeddedId
    private SaldoClienteEntityId id;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private TipoContratacao tipoContratacao;

    @Column(nullable = false)
    private int quantidade;

    @Column(nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private LocalDate vencimento;

    @ManyToOne
    @JoinColumn(name = "ID_CLIENTE", nullable = false, insertable = false, updatable = false)
    private ClientesEntity cliente;

    @ManyToOne
    @JoinColumn(name = "ID_ESPACO", nullable = false, insertable = false, updatable = false)
    private EspacosEntity espaco;

    @OneToMany( mappedBy = "saldoCliente")
    private List<AcessosEntity> acesso;


    @Override
    public SaldoClienteEntityId getId() {
        return id;
    }

    @Override
    public void setId(SaldoClienteEntityId id) {
        this.id = id;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public LocalDate getVencimento() {
        return vencimento;
    }

    public void setVencimento(LocalDate vencimento) {
        this.vencimento = vencimento;
    }

    public ClientesEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClientesEntity cliente) {
        this.cliente = cliente;
    }

    public EspacosEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacosEntity espaco) {
        this.espaco = espaco;
    }

    public List<AcessosEntity> getAcesso() {
        return acesso;
    }

    public void setAcesso(List<AcessosEntity> acesso) {
        this.acesso = acesso;
    }
}
