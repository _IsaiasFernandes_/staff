package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.EspacosEntity;
import br.com.dbccompany.coworking.Entity.EspacosPacotesEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.Utils.TratarValor;

import java.util.List;

public class EspacosDTO {

    private Integer id;
    private String nome;
    private int qtdPessoas;
    private String valor;
    private List<ContratacaoEntity> contratacao;
    private List<EspacosPacotesEntity> espacosPacotes;
    private List<SaldoClienteEntity> saldoCliente;

    public EspacosDTO() {
    }

    public EspacosDTO(EspacosEntity espacos) {
        this.id = espacos.getId();
        this.nome = espacos.getNome();
        this.qtdPessoas = espacos.getQtdPessoas();
        this.valor = TratarValor.valorString(espacos.getValor());
        this.contratacao = espacos.getContratacao();
        this.espacosPacotes = espacos.getEspacosPacote();
        this.saldoCliente = espacos.getSaldoCliente();
    }

    public EspacosEntity convert() {
        EspacosEntity espacos = new EspacosEntity();
        espacos.setId(this.id);
        espacos.setNome(this.nome);
        espacos.setQtdPessoas(this.qtdPessoas);
        espacos.setValor(TratarValor.valorDouble(this.valor));
        espacos.setContratacao(this.contratacao);
        espacos.setEspacosPacote(this.espacosPacotes);
        espacos.setSaldoCliente(this.saldoCliente);

        return espacos;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(int qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public List<ContratacaoEntity> getContratacao() {
        return contratacao;
    }

    public void setContratacao(List<ContratacaoEntity> contratacao) {
        this.contratacao = contratacao;
    }

    public List<EspacosPacotesEntity> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacosPacotesEntity> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public List<SaldoClienteEntity> getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(List<SaldoClienteEntity> saldoCliente) {
        this.saldoCliente = saldoCliente;
    }
}
