package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class SaldoClienteEntityId implements Serializable {

    @Column(name = "ID_CLIENTE")
    private int cliente;

    @Column( name = "ID_ESPACO")
    private int espaco;

    public SaldoClienteEntityId() {
    }

    public SaldoClienteEntityId(int cliente, int espaco) {
        this.cliente = cliente;
        this.espaco = espaco;
    }


    public int getCliente() {
        return cliente;
    }

    public void setCliente(int cliente) {
        this.cliente = cliente;
    }

    public int getEspaco() {
        return espaco;
    }

    public void setEspaco(int espaco) {
        this.espaco = espaco;
    }
}
