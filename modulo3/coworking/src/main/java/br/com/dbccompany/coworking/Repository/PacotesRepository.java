package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.PacotesEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PacotesRepository extends CrudRepository<PacotesEntity, Integer> {

    PacotesEntity findByValor(double valor);
    Optional<PacotesEntity> findById(Integer id);
}
