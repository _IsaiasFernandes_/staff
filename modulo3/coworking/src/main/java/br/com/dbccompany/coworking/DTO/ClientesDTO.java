package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.*;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.sql.Date;
import java.util.List;

public class ClientesDTO {

    private Integer id;
    private String nome;
    private String cpf;
    private Date dataNascimento;
    private List<ClientesPacotesEntity> clientesPacotes;
    private List<ContratacaoEntity> contratacao;
    private List<SaldoClienteEntity> saldoCliente;
    private List<ContatoEntity> contato;

    public ClientesDTO() {
    }

    public ClientesDTO(ClientesEntity clientes) {
        this.id = clientes.getId();
        this.nome = clientes.getNome();
        this.cpf = clientes.getCpf();
        this.dataNascimento = clientes.getDataNascimento();
        this.clientesPacotes = clientes.getClientesPacotes();
        this.contratacao = clientes.getContratacao();
        this.saldoCliente = clientes.getSaldoCliente();
        this.contato = clientes.getContato();
    }

    public ClientesEntity convert() {
        ClientesEntity clientes = new ClientesEntity();
        clientes.setId(this.id);
        clientes.setNome(this.nome);
        clientes.setCpf(this.cpf);
        clientes.setDataNascimento(this.dataNascimento);
        clientes.setClientesPacotes(this.clientesPacotes);
        clientes.setContratacao(this.contratacao);
        clientes.setSaldoCliente(this.saldoCliente);
        clientes.setContato(this.contato);

        return clientes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public List<ClientesPacotesEntity> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<ClientesPacotesEntity> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public List<ContratacaoEntity> getContratacao() {
        return contratacao;
    }

    public void setContratacao(List<ContratacaoEntity> contratacao) {
        this.contratacao = contratacao;
    }

    public List<SaldoClienteEntity> getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(List<SaldoClienteEntity> saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public List<ContatoEntity> getContato() {
        return contato;
    }

    public void setContato(List<ContatoEntity> contato) {
        this.contato = contato;
    }
}
