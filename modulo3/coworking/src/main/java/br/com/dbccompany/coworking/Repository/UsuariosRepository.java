package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.UsuariosEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UsuariosRepository extends CrudRepository<UsuariosEntity, Integer> {

    UsuariosEntity findByUsernameAndPassword(String username, String password);
    Optional<UsuariosEntity> findByNome(String nome);
    Optional<UsuariosEntity> findByUsername(String username);
    Optional<UsuariosEntity> findById(Integer id);
}
