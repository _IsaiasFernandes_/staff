package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.TipoContatoDTO;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import br.com.dbccompany.coworking.Repository.TipoContatoRepository;
import org.springframework.stereotype.Service;

@Service
public class TipoContatoService extends ServiceAbstract<TipoContatoRepository, TipoContatoEntity, Integer>{
}
