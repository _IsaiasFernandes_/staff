package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.PagamentosDTO;
import br.com.dbccompany.coworking.Entity.PagamentosEntity;
import br.com.dbccompany.coworking.Service.PagamentosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/pagamentos")
public class PagamentosController {

    @Autowired
    PagamentosService service;


    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public PagamentosDTO Especifico(@PathVariable Integer id){
        PagamentosDTO PagamentosDTO = new PagamentosDTO(service.porId(id));
        return PagamentosDTO;
    }

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<PagamentosDTO> todos() {
        List<PagamentosDTO> PagamentosDTOS= new ArrayList<>();
        for(PagamentosEntity pagamento : service.todos()){
            PagamentosDTOS.add(new PagamentosDTO(pagamento));
        }
        return PagamentosDTOS;
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public PagamentosDTO salvar(@RequestBody PagamentosDTO entity){
        PagamentosEntity PagamentosEntity = entity.convert();
        PagamentosDTO pagamentosDTO = new PagamentosDTO(service.salvar(PagamentosEntity));
        return pagamentosDTO;
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public PagamentosDTO editar(@PathVariable Integer id, @RequestBody PagamentosDTO entity){
        PagamentosEntity pagamentosEntity = entity.convert();
        PagamentosDTO pagamentosDTO = new PagamentosDTO(service.editar(pagamentosEntity,id));
        return pagamentosDTO;
    }
}
