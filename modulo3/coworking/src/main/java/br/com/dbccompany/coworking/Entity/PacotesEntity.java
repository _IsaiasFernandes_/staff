package br.com.dbccompany.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.List;

@Entity
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = PacotesEntity.class)
public class PacotesEntity extends EntityAbstract<Integer>{

    @Id
    @SequenceGenerator(name = "PACOTES_SEQ", sequenceName = "PACOTES_SEQ")
    @GeneratedValue( generator = "PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private double valor;

    @OneToMany(mappedBy = "pacote")
    private List<ClientesPacotesEntity> clientesPacote;

    @OneToMany(mappedBy = "pacote")
    private List<EspacosPacotesEntity> espacosPacote;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public List<ClientesPacotesEntity> getClientesPacote() {
        return clientesPacote;
    }

    public void setClientesPacote(List<ClientesPacotesEntity> clientesPacote) {
        this.clientesPacote = clientesPacote;
    }

    public List<EspacosPacotesEntity> getEspacosPacote() {
        return espacosPacote;
    }

    public void setEspacosPacote(List<EspacosPacotesEntity> espacosPacote) {
        this.espacosPacote = espacosPacote;
    }
}
