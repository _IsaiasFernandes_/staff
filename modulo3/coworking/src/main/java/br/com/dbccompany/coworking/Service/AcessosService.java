package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.CoworkingApplication;
import br.com.dbccompany.coworking.DTO.AcessosDTO;
import br.com.dbccompany.coworking.Entity.AcessosEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntityId;
import br.com.dbccompany.coworking.Repository.AcessosRepository;
import br.com.dbccompany.coworking.Repository.SaldoClienteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AcessosService extends ServiceAbstract<AcessosRepository, AcessosEntity, Integer>{

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @Autowired
    SaldoClienteRepository saldoRepository;

    public AcessosDTO salvarEntrada( AcessosDTO acesso ){

        if( acesso.isEntrada() ) {
            SaldoClienteEntityId id = acesso.getSaldoCliente().getId();
            SaldoClienteEntity saldo = saldoRepository.findById(id).get();
            if(saldo.getQuantidade() <= 0) {
                return null;
            }
        }
        AcessosEntity acessoEntity = acesso.convert();
        AcessosDTO newDto = new AcessosDTO(super.salvar(acessoEntity));
        return newDto;
    }
}
