package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.CoworkingApplication;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntityId;
import br.com.dbccompany.coworking.Repository.SaldoClienteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class SaldoClienteService extends ServiceAbstract<SaldoClienteRepository, SaldoClienteEntity, SaldoClienteEntityId>{

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);


    @Transactional(rollbackFor = Exception.class)
    public SaldoClienteEntity editar(SaldoClienteEntity entidade, Integer idCliente, Integer idEspaco) {
        try {
            SaldoClienteEntityId newId = new SaldoClienteEntityId(idCliente, idEspaco);
            entidade.setId(newId);
            return repository.save(entidade);
        } catch (Exception e) {
            logger.error("Erro ao editar saldo cliente: " + e.getMessage());
            throw new RuntimeException();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public Optional<SaldoClienteEntity> porId(Integer idCliente, Integer idEspaco) {
        try {
            SaldoClienteEntityId newId = new SaldoClienteEntityId(idCliente, idEspaco);
            Optional<SaldoClienteEntity> optionalEntity = repository.findById(newId);
            return optionalEntity;
        } catch (Exception e) {
            logger.error("Erro ao buscar saldo cliente por id: " + e.getMessage());
            throw new RuntimeException();
        }
    }
}
