package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Utils.TratarValor;
import br.com.dbccompany.coworking.Entity.ClientesPacotesEntity;
import br.com.dbccompany.coworking.Entity.EspacosPacotesEntity;
import br.com.dbccompany.coworking.Entity.PacotesEntity;

import java.util.List;

public class PacotesDTO {

    private Integer id;
    private String valor;
    private List<EspacosPacotesEntity> espacosPacotes;
    private List<ClientesPacotesEntity> clientesPacotes;

    public PacotesDTO() {
    }

    public PacotesDTO(PacotesEntity pacote) {
        this.id = pacote.getId();
        this.espacosPacotes = pacote.getEspacosPacote();
        this.valor = TratarValor.valorString(pacote.getValor());
        this.clientesPacotes = pacote.getClientesPacote();
    }

    public PacotesEntity convert() {
        PacotesEntity pacote = new PacotesEntity();
        pacote.setId(this.id);
        pacote.setEspacosPacote(this.espacosPacotes);
        pacote.setValor(TratarValor.valorDouble(this.valor));
        pacote.setClientesPacote(this.clientesPacotes);
        return pacote;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<EspacosPacotesEntity> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacosPacotesEntity> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public List<ClientesPacotesEntity> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<ClientesPacotesEntity> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }
}
