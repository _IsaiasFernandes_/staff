package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity

public class EspacosEntity extends EntityAbstract<Integer>{

    @Id
    @SequenceGenerator(name = "ESPACOS_SEQ", sequenceName = "ESPACOS_SEQ")
    @GeneratedValue( generator = "ESPACOS_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(nullable = false, unique = true)
    private String nome;

    @Column(nullable = false)
    private int qtdPessoas;

    @Column(nullable = false)
    private double valor;

    @OneToMany(mappedBy = "espaco")
    private List<ContratacaoEntity> contratacao;

    @OneToMany(mappedBy = "espaco")
    private List<EspacosPacotesEntity> espacosPacote;

    @OneToMany(mappedBy = "espaco")
    private List<SaldoClienteEntity> saldoCliente;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(int qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public List<ContratacaoEntity> getContratacao() {
        return contratacao;
    }

    public void setContratacao(List<ContratacaoEntity> contratacao) {
        this.contratacao = contratacao;
    }

    public List<EspacosPacotesEntity> getEspacosPacote() {
        return espacosPacote;
    }

    public void setEspacosPacote(List<EspacosPacotesEntity> espacosPacote) {
        this.espacosPacote = espacosPacote;
    }

    public List<SaldoClienteEntity> getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(List<SaldoClienteEntity> saldoCliente) {
        this.saldoCliente = saldoCliente;
    }
}
