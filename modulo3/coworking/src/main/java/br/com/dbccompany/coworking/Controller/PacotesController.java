package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.PacotesDTO;
import br.com.dbccompany.coworking.Entity.PacotesEntity;
import br.com.dbccompany.coworking.Service.PacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/pacotes")
public class PacotesController {

    @Autowired
    PacotesService service;


    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public PacotesDTO Especifico(@PathVariable Integer id){
        PacotesDTO PacotesDTO = new PacotesDTO(service.porId(id));
        return PacotesDTO;
    }

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<PacotesDTO> todos() {
        List<PacotesDTO> PacotesDTOS= new ArrayList<>();
        for(PacotesEntity pacotes : service.todos()){
            PacotesDTOS.add(new PacotesDTO(pacotes));
        }
        return PacotesDTOS;
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public PacotesDTO salvar(@RequestBody PacotesDTO entity){
        PacotesEntity pacotesEntity = entity.convert();
        PacotesDTO pacotesDTO = new PacotesDTO(service.salvar(pacotesEntity));
        return pacotesDTO;
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public PacotesDTO editar(@PathVariable Integer id, @RequestBody PacotesDTO entity){
        PacotesEntity pacotesEntity = entity.convert();
        PacotesDTO pacotesDTO = new PacotesDTO(service.editar(pacotesEntity,id));
        return pacotesDTO;
    }

}
