package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.EspacosEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EspacosRepository extends CrudRepository<EspacosEntity, Integer> {

    Optional<EspacosEntity> findById(Integer id);
    EspacosEntity findByQtdPessoasAndValor(int qtdPessoas, double valor);
    EspacosEntity findByNome(String nome);
}
