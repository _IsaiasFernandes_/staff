package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ClientesEntity;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.Enum.TipoContratacao;
import br.com.dbccompany.coworking.Entity.EspacosEntity;
import br.com.dbccompany.coworking.Entity.PagamentosEntity;

import java.util.List;

public class ContratacaoDTO {

    private Integer id;
    private TipoContratacao tipoContratacao;
    private int quantidade;
    private double desconto;
    private double prazo;
    private List<PagamentosEntity> pagamentos;
    private ClientesEntity clientes;
    private EspacosEntity espacos;
    private double valorContrato;

    {
        this.desconto = 0.0;
    }

    public ContratacaoDTO() {
    }

    public ContratacaoDTO(ContratacaoEntity contratacao) {
        this.id = contratacao.getId();
        this.tipoContratacao = contratacao.getTipoContratacao();
        this.quantidade = contratacao.getQuantidade();
        this.desconto = contratacao.getDesconto();
        this.prazo = contratacao.getPrazo();
        this.pagamentos = contratacao.getPagamentos();
        this.clientes = contratacao.getCliente();
        this.espacos = contratacao.getEspaco();
        this.valorContrato = contratacao.getEspaco().getValor() - this.desconto;
    }

    public ContratacaoEntity convert() {
        ContratacaoEntity contratacao = new ContratacaoEntity();

        contratacao.setId(this.id);
        contratacao.setTipoContratacao(this.tipoContratacao);
        contratacao.setQuantidade(this.quantidade);
        contratacao.setDesconto(this.desconto);
        contratacao.setPrazo(this.prazo);
        contratacao.setPagamentos(this.pagamentos);
        contratacao.setCliente(this.clientes);
        contratacao.setEspaco(this.espacos);

        return contratacao;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public double getDesconto() {
        return desconto;
    }

    public void setDesconto(double desconto) {
        this.desconto = desconto;
    }

    public double getPrazo() {
        return prazo;
    }

    public void setPrazo(double prazo) {
        this.prazo = prazo;
    }

    public List<PagamentosEntity> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<PagamentosEntity> pagamentos) {
        this.pagamentos = pagamentos;
    }

    public ClientesEntity getClientes() {
        return clientes;
    }

    public void setClientes(ClientesEntity clientes) {
        this.clientes = clientes;
    }

    public EspacosEntity getEspacos() {
        return espacos;
    }

    public void setEspacos(EspacosEntity espacos) {
        this.espacos = espacos;
    }

    public double getValorContrato() {
        return valorContrato;
    }

    public void setValorContrato(double valorContrato) {
        this.valorContrato = valorContrato;
    }
}
