package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;

import java.util.List;

public class TipoContatoDTO {

    private Integer id;
    private String nome;
    private List<ContatoEntity> contatos;

    public TipoContatoDTO() {
    }

    public TipoContatoDTO(TipoContatoEntity tipoContato) {
        this.id = tipoContato.getId();
        this.nome = tipoContato.getNome();
        this.contatos = tipoContato.getContato();
    }

    public TipoContatoEntity convert() {
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setId(this.id);
        tipoContato.setNome(this.nome);
        tipoContato.setContato(this.contatos);
        return tipoContato;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<ContatoEntity> getContatos() {
        return contatos;
    }

    public void setContatos(List<ContatoEntity> contatos) {
        this.contatos = contatos;
    }
}
