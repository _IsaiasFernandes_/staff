package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.UsuariosEntity;

public class UsuariosDTO {

    private Integer id;
    private String nome;
    private String email;
    private String username;
    private String password;

    public UsuariosDTO() {
    }

    public UsuariosDTO(UsuariosEntity usuario) {
        this.id = usuario.getId();
        this.nome = usuario.getNome();
        this.email = usuario.getEmail();
        this.username = usuario.getUsername();
        this.password = usuario.getPassword();
    }

    public UsuariosEntity convert() {
        UsuariosEntity usuario = new UsuariosEntity();
        usuario.setId(this.id);
        usuario.setNome(this.nome);
        usuario.setEmail(this.email);
        usuario.setUsername(this.username);
        usuario.setPassword(this.password);
        return usuario;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
