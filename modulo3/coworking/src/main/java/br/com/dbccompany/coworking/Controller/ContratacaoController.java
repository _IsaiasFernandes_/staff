package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ContratacaoDTO;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Service.ContratacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/contratacao")
public class ContratacaoController {

    @Autowired
    ContratacaoService service;


    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public ContratacaoDTO Especifico(@PathVariable Integer id){
        ContratacaoDTO contratacaoDTO = new ContratacaoDTO(service.porId(id));
        return contratacaoDTO;
    }

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<ContratacaoDTO> todos() {
        List<ContratacaoDTO> ContratacaoDTOS= new ArrayList<>();
        for(ContratacaoEntity acesso : service.todos()){
            ContratacaoDTOS.add(new ContratacaoDTO(acesso));
        }
        return ContratacaoDTOS;
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public ContratacaoDTO salvar(@RequestBody ContratacaoDTO entity){
        ContratacaoEntity contratacaoEntity = entity.convert();
        ContratacaoDTO contratacaoDTO = new ContratacaoDTO(service.salvar(contratacaoEntity));
        return contratacaoDTO;
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public ContratacaoDTO editar(@PathVariable Integer id, @RequestBody ContratacaoDTO entity){
        ContratacaoEntity contratacaoEntity = entity.convert();
        ContratacaoDTO contratacaoDTO = new ContratacaoDTO(service.editar(contratacaoEntity,id));
        return contratacaoDTO;
    }
}
