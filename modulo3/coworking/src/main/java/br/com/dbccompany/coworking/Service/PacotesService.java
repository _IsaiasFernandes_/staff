package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.PacotesDTO;
import br.com.dbccompany.coworking.Entity.PacotesEntity;
import br.com.dbccompany.coworking.Repository.PacotesRepository;
import org.springframework.stereotype.Service;

@Service
public class PacotesService extends ServiceAbstract<PacotesRepository, PacotesEntity, Integer>{
}
