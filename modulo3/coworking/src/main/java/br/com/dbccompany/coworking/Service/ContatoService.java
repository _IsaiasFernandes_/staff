package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ContatoDTO;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Repository.ContatoRepository;
import org.springframework.stereotype.Service;

@Service
public class ContatoService extends ServiceAbstract<ContatoRepository, ContatoEntity, Integer>{
}
