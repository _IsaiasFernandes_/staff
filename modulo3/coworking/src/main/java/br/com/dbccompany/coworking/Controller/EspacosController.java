package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.EspacosDTO;
import br.com.dbccompany.coworking.Entity.EspacosEntity;
import br.com.dbccompany.coworking.Service.EspacosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/espacos")
public class EspacosController {

    @Autowired
    EspacosService service;


    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public EspacosDTO Especifico(@PathVariable Integer id){
        EspacosDTO espacosDTO = new EspacosDTO(service.porId(id));
        return espacosDTO;
    }

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<EspacosDTO> todos() {
        List<EspacosDTO> espacosDTOS= new ArrayList<>();
        for(EspacosEntity acesso : service.todos()){
            espacosDTOS.add(new EspacosDTO(acesso));
        }
        return espacosDTOS;
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public EspacosDTO salvar(@RequestBody EspacosDTO entity){
        EspacosEntity espacosEntity = entity.convert();
        EspacosDTO espacosDTO = new EspacosDTO(service.salvar(espacosEntity));
        return espacosDTO;
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public EspacosDTO editar(@PathVariable Integer id, @RequestBody EspacosDTO entity){
        EspacosEntity espacosEntity = entity.convert();
        EspacosDTO espacosDTO = new EspacosDTO(service.editar(espacosEntity,id));
        return espacosDTO;
    }
}
