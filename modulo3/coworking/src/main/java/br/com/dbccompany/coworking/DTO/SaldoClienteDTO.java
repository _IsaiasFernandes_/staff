package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Entity.Enum.TipoContratacao;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

public class SaldoClienteDTO {

    private SaldoClienteEntityId id;
    private TipoContratacao tipoContratacao;
    private int quantidade;
    private LocalDate vencimento;
    private ClientesEntity clientes;
    private EspacosEntity espacos;
    private List<AcessosEntity> acessos;

    public SaldoClienteDTO() {
    }

    public SaldoClienteDTO(SaldoClienteEntity saldoCliente) {
        this.id = saldoCliente.getId();
        this.tipoContratacao = saldoCliente.getTipoContratacao();
        this.quantidade = saldoCliente.getQuantidade();
        this.vencimento = saldoCliente.getVencimento();
        this.clientes = saldoCliente.getCliente();
        this.espacos = saldoCliente.getEspaco();
        this.acessos = saldoCliente.getAcesso();
    }

    public SaldoClienteEntity convert() {
        SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
        saldoCliente.setId(this.id);
        saldoCliente.setTipoContratacao(this.tipoContratacao);
        saldoCliente.setQuantidade(this.quantidade);
        saldoCliente.setVencimento(this.vencimento);
        saldoCliente.setCliente(this.clientes);
        saldoCliente.setEspaco(this.espacos);
        saldoCliente.setAcesso( this.acessos);
        return saldoCliente;
    }


    public SaldoClienteEntityId getId() {
        return id;
    }

    public void setId(SaldoClienteEntityId id) {
        this.id = id;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public LocalDate getVencimento() {
        return vencimento;
    }

    public void setVencimento(LocalDate vencimento) {
        this.vencimento = vencimento;
    }

    public ClientesEntity getClientes() {
        return clientes;
    }

    public void setClientes(ClientesEntity clientes) {
        this.clientes = clientes;
    }

    public EspacosEntity getEspacos() {
        return espacos;
    }

    public void setEspacos(EspacosEntity espacos) {
        this.espacos = espacos;
    }

    public List<AcessosEntity> getAcessos() {
        return acessos;
    }

    public void setAcessos(List<AcessosEntity> acessos) {
        this.acessos = acessos;
    }
}
