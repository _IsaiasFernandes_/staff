package br.com.dbccompany.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.List;

@Entity
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = ContatoEntity.class)
public class ContatoEntity extends EntityAbstract<Integer>{

    @Id
    @SequenceGenerator(name = "TIPO_CONTATO_SEQ", sequenceName = "TIPO_CONTATO_SEQ")
    @GeneratedValue( generator = "TIPO_CONTATO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private String valor;

    @ManyToOne
    @JoinColumn(name = "ID_TIPO_CONTATO")
    private TipoContatoEntity tipoContato;

    @ManyToOne
    @JoinColumn(name = "ID_CLIENTE")
    private ClientesEntity cliente;


    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public ClientesEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClientesEntity cliente) {
        this.cliente = cliente;
    }

    public TipoContatoEntity getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContatoEntity tipoContato) {
        this.tipoContato = tipoContato;
    }


}
