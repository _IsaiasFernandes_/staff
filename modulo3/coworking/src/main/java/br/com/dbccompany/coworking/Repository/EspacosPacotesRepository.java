package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.EspacosEntity;
import br.com.dbccompany.coworking.Entity.EspacosPacotesEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EspacosPacotesRepository extends CrudRepository<EspacosPacotesEntity, Integer> {

    Optional<EspacosEntity> findAllById(Integer id);
}
