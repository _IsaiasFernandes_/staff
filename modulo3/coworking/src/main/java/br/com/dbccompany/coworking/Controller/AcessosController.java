package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.AcessosDTO;
import br.com.dbccompany.coworking.Entity.AcessosEntity;
import br.com.dbccompany.coworking.Service.AcessosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/acessos")
public class AcessosController {

    @Autowired
    AcessosService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<AcessosDTO> todosAcesso() {
        List<AcessosDTO> listaDTO = new ArrayList<>();
        for (AcessosEntity acesso : service.todos()) {
            listaDTO.add(new AcessosDTO(acesso));
        }
        return listaDTO;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public AcessosDTO salvar(@RequestBody AcessosDTO acessosDTO){
        return service.salvarEntrada(acessosDTO);
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public AcessosDTO acessoEspecifico(@PathVariable Integer id) {
        AcessosDTO acessosDTO = new AcessosDTO(service.porId(id));
        return acessosDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public AcessosDTO editarAcesso(@PathVariable Integer id, @RequestBody AcessosDTO acessosDTO) {
        AcessosEntity acesso = acessosDTO.convert();
        AcessosDTO newDTO = new AcessosDTO(service.editar(acesso, id));
        return newDTO;
    }
}
