package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.UsuariosDTO;
import br.com.dbccompany.coworking.Entity.UsuariosEntity;
import br.com.dbccompany.coworking.Service.UsuariosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/usuarios")
public class UsuariosController {

    @Autowired
    UsuariosService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<UsuariosDTO> todosUsuarios() {
        return service.retornarListaUsuarios();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public UsuariosDTO salvar(@RequestBody UsuariosDTO user) {
        UsuariosEntity usuariosEntity = user.convert();
        UsuariosDTO newDto = new UsuariosDTO(service.salvar(usuariosEntity));
        return newDto;
    }

    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public UsuariosEntity usuariosEspecifico(@PathVariable Integer id){
        return service.porId(id);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public UsuariosDTO editarUsuarios(@PathVariable Integer id, @RequestBody UsuariosDTO usuario){
        UsuariosEntity usuariosEntity = usuario.convert();
        UsuariosDTO usuariosDTO = new UsuariosDTO(service.editar(usuariosEntity, id));
        return usuariosDTO;
    }

    @PostMapping(value = "/login" )
    @ResponseBody
    public boolean fazerLogin(@RequestBody UsuariosDTO login) {
        return service.login(login.getUsername(), login.getPassword());
    }

}
