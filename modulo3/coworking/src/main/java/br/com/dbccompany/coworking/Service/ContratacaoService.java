package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ContratacaoDTO;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Repository.ContratacaoRepository;
import org.springframework.stereotype.Service;

@Service
public class ContratacaoService extends ServiceAbstract<ContratacaoRepository, ContratacaoEntity, Integer>{
}
