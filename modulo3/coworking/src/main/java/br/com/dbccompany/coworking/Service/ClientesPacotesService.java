package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ClientesPacotesDTO;
import br.com.dbccompany.coworking.Entity.ClientesPacotesEntity;
import br.com.dbccompany.coworking.Repository.ClientesPacotesRepository;
import org.springframework.stereotype.Service;

@Service
public class ClientesPacotesService extends ServiceAbstract<ClientesPacotesRepository, ClientesPacotesEntity, Integer>{
}
