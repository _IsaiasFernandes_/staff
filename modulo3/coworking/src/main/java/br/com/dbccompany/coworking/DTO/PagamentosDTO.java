package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ClientesPacotesEntity;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.Enum.TipoPagamento;
import br.com.dbccompany.coworking.Entity.PagamentosEntity;

public class PagamentosDTO {

    private Integer id;
    private ClientesPacotesEntity clientesPacotes;
    private ContratacaoEntity contratacao;
    private TipoPagamento tipoPagamento;

    public PagamentosDTO() {
    }

    public PagamentosDTO(PagamentosEntity pagamento) {
        this.id = pagamento.getId();
        this.clientesPacotes = pagamento.getClientePacote();
        this.contratacao = pagamento.getContratacao();
        this.tipoPagamento = pagamento.getTipoPagamento();
    }

    public PagamentosEntity convert() {
        PagamentosEntity pagamento = new PagamentosEntity();
        pagamento.setId(this.id);
        pagamento.setClientePacote(this.clientesPacotes);
        pagamento.setContratacao(this.contratacao);
        pagamento.setTipoPagamento(this.tipoPagamento);
        return pagamento;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClientesPacotesEntity getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(ClientesPacotesEntity clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public ContratacaoEntity getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContratacaoEntity contratacao) {
        this.contratacao = contratacao;
    }

    public TipoPagamento getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamento tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }
}
