package br.com.dbccompany.coworking.Entity.Enum;

public enum TipoContratacao {
    MINUTO, HORA, TURNO, DIARIA, SEMANA, MES
}
