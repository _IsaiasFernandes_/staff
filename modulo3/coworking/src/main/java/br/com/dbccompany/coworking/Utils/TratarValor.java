package br.com.dbccompany.coworking.Utils;

import java.text.NumberFormat;
import java.util.Locale;

public class TratarValor {

    public static String valorString(Double valor) {
        Locale BR = new Locale("pt","BR");
        NumberFormat nf = NumberFormat.getCurrencyInstance(BR);
        return nf.format(valor);
    }

    public static Double valorDouble(String valorString) {
        String aux = valorString.replaceAll("\\.", "");
        aux = aux.replaceAll("R\\$", "");
        aux = aux.replaceAll(" ", "");
        aux = aux.replaceAll(",", ".");
        return Double.parseDouble(aux);
    }
}
