package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.TipoContatoDTO;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import br.com.dbccompany.coworking.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/tipo_contato")
public class TipoContatoController {

    @Autowired
    TipoContatoService service;


    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public TipoContatoDTO Especifico(@PathVariable Integer id){
        TipoContatoDTO tipoContatoDTO = new TipoContatoDTO(service.porId(id));
        return tipoContatoDTO;
    }

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<TipoContatoDTO> todos() {
        List<TipoContatoDTO> tipoContatoDTOS= new ArrayList<>();
        for(TipoContatoEntity tipoContato : service.todos()){
            tipoContatoDTOS.add(new TipoContatoDTO(tipoContato));
        }
        return tipoContatoDTOS;
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public TipoContatoDTO salvar(@RequestBody TipoContatoDTO entity){
        TipoContatoEntity tipoContatoEntity = entity.convert();
        TipoContatoDTO tipoContatoDTO = new TipoContatoDTO(service.salvar(tipoContatoEntity));
        return tipoContatoDTO;
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public TipoContatoDTO editar(@PathVariable Integer id, @RequestBody TipoContatoDTO entity){
        TipoContatoEntity tipoContatoEntity = entity.convert();
        TipoContatoDTO tipoContatoDTO = new TipoContatoDTO(service.editar(tipoContatoEntity,id));
        return tipoContatoDTO;
    }
}
