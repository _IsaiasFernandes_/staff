package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.EspacosPacotesEntity;
import br.com.dbccompany.coworking.Repository.EspacosPacotesRepository;
import org.springframework.stereotype.Service;

@Service
public class EspacosPacotesService extends ServiceAbstract<EspacosPacotesRepository, EspacosPacotesEntity, Integer>{
}
