package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ClientesEntity;
import br.com.dbccompany.coworking.Entity.ClientesPacotesEntity;
import br.com.dbccompany.coworking.Entity.PacotesEntity;
import br.com.dbccompany.coworking.Entity.PagamentosEntity;

import java.util.List;

public class ClientesPacotesDTO {

    private Integer id;
    private int quantidade;
    private List<PagamentosEntity> pagamentos;
    private ClientesEntity clientes;
    private PacotesEntity pacotes;

    public ClientesPacotesDTO() {
    }

    public ClientesPacotesDTO(ClientesPacotesEntity clientesPacotes) {
        this.id = clientesPacotes.getId();
        this.quantidade = clientesPacotes.getQuantidade();
        this.pagamentos = clientesPacotes.getPagamento();
        this.clientes = clientesPacotes.getCliente();
        this.pacotes = clientesPacotes.getPacote();
    }

    public ClientesPacotesEntity convert() {
        ClientesPacotesEntity clientesPacotes = new ClientesPacotesEntity();
        clientesPacotes.setId(this.id);
        clientesPacotes.setQuantidade(this.quantidade);
        clientesPacotes.setPagamento(this.pagamentos);
        clientesPacotes.setCliente(this.clientes);
        clientesPacotes.setPacote(this.pacotes);

        return clientesPacotes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public List<PagamentosEntity> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<PagamentosEntity> pagamentos) {
        this.pagamentos = pagamentos;
    }

    public ClientesEntity getClientes() {
        return clientes;
    }

    public void setClientes(ClientesEntity clientes) {
        this.clientes = clientes;
    }

    public PacotesEntity getPacotes() {
        return pacotes;
    }

    public void setPacotes(PacotesEntity pacotes) {
        this.pacotes = pacotes;
    }
}
