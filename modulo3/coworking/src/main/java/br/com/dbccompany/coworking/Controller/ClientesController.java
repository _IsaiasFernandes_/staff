package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ClientesDTO;
import br.com.dbccompany.coworking.Entity.ClientesEntity;
import br.com.dbccompany.coworking.Service.ClientesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/clientes")
public class ClientesController{

    @Autowired
    ClientesService service;


    @GetMapping(value = "/todos")
    @ResponseBody
    public List<ClientesDTO> todosCliente() {
        List<ClientesDTO> listaDTO = new ArrayList<>();
        for (ClientesEntity cliente : service.todos()) {
            listaDTO.add(new ClientesDTO(cliente));
        }
        return listaDTO;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public ClientesDTO salvar(@RequestBody ClientesDTO clientesDTO) {
        ClientesEntity clientesEntity = clientesDTO.convert();
        ClientesDTO newDto = new ClientesDTO(service.salvar(clientesEntity));
        return newDto;
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public ClientesDTO clienteEspecifico(@PathVariable Integer id) {
        ClientesDTO clientesDTO = new ClientesDTO(service.porId(id));
        return clientesDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ClientesDTO editarCliente(@PathVariable Integer id, @RequestBody ClientesDTO clientesDTO) {
        ClientesEntity cliente = clientesDTO.convert();
        ClientesDTO newDTO = new ClientesDTO(service.editar(cliente, id));
        return newDTO;
    }
}
