package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.AcessosEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.sql.Date;

public class AcessosDTO {

    private Integer id;
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private SaldoClienteEntity saldoCliente;
    private boolean isEntrada;
    private Date data;
    private boolean isExcecao;

    {
        this.isEntrada = false;
        this.isExcecao = false;
    }

    public AcessosDTO() {
    }

    public AcessosDTO(AcessosEntity acessos) {
        this.id = acessos.getId();
        this.saldoCliente = acessos.getSaldoCliente();
        this.isEntrada = acessos.isEntrada();
        this.data = acessos.getData();
        this.isExcecao = acessos.isExcecao();
    }

    public AcessosEntity convert() {
        AcessosEntity acessos = new AcessosEntity();
        acessos.setId(this.id);
        acessos.setSaldoCliente(this.saldoCliente);
        acessos.setEntrada(this.isEntrada);
        acessos.setData(this.data);
        acessos.setExcecao(this.isExcecao);

        return acessos;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SaldoClienteEntity getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoClienteEntity saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public boolean isEntrada() {
        return isEntrada;
    }

    public void setEntrada(boolean entrada) {
        isEntrada = entrada;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public boolean isExcecao() {
        return isExcecao;
    }

    public void setExcecao(boolean excecao) {
        isExcecao = excecao;
    }
}
