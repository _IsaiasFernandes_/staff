package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClientesEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ClientesRepository extends CrudRepository<ClientesEntity, Integer> {

    ClientesEntity findByCpf(String cpf);
    ClientesEntity findByNomeAndCpf(String nome, String cpf);
    Optional<ClientesEntity> findById(Integer id);
}
