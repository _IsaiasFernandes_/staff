package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ContatoDTO;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/contato")
public class ContatoController {

    @Autowired
    ContatoService service;


    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public ContatoDTO Especifico(@PathVariable Integer id){
        ContatoDTO contatoDTO = new ContatoDTO(service.porId(id));
        return contatoDTO;
    }

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<ContatoDTO> todos() {
        List<ContatoDTO> ContatoDTOS= new ArrayList<>();
        for(ContatoEntity acesso : service.todos()){
            ContatoDTOS.add(new ContatoDTO(acesso));
        }
        return ContatoDTOS;
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public ContatoDTO salvar(@RequestBody ContatoDTO entity){
        ContatoEntity contatoEntity = entity.convert();
        ContatoDTO contatoDTO = new ContatoDTO(service.salvar(contatoEntity));
        return contatoDTO;
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public ContatoDTO editar(@PathVariable Integer id, @RequestBody ContatoDTO entity){
        ContatoEntity contatoEntity = entity.convert();
        ContatoDTO contatoDTO = new ContatoDTO(service.editar(contatoEntity,id));
        return contatoDTO;
    }
}
