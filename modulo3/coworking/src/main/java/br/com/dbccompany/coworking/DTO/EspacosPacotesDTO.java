package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.Enum.TipoContratacao;
import br.com.dbccompany.coworking.Entity.EspacosEntity;
import br.com.dbccompany.coworking.Entity.EspacosPacotesEntity;
import br.com.dbccompany.coworking.Entity.PacotesEntity;

public class EspacosPacotesDTO {

    private Integer id;
    private TipoContratacao tipoContratacao;
    private int quantidade;
    private double prazo;
    private EspacosEntity espacos;
    private PacotesEntity pacotes;

    public EspacosPacotesDTO() {
    }

    public EspacosPacotesDTO(EspacosPacotesEntity espacosPacotes) {
        this.id = espacosPacotes.getId();
        this.tipoContratacao = espacosPacotes.getTipoContratacao();
        this.quantidade = espacosPacotes.getQuantidade();
        this.prazo = espacosPacotes.getPrazo();
        this.espacos = espacosPacotes.getEspaco();
        this.pacotes = espacosPacotes.getPacote();
    }

    public EspacosPacotesEntity convert() {
        EspacosPacotesEntity espacosPacotes = new EspacosPacotesEntity();

        espacosPacotes.setId(this.id);
        espacosPacotes.setTipoContratacao(this.tipoContratacao);
        espacosPacotes.setQuantidade(this.quantidade);
        espacosPacotes.setPrazo(this.prazo);
        espacosPacotes.setEspaco(this.espacos);
        espacosPacotes.setPacote(this.pacotes);

        return espacosPacotes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public double getPrazo() {
        return prazo;
    }

    public void setPrazo(double prazo) {
        this.prazo = prazo;
    }

    public EspacosEntity getEspacos() {
        return espacos;
    }

    public void setEspacos(EspacosEntity espacos) {
        this.espacos = espacos;
    }

    public PacotesEntity getPacotes() {
        return pacotes;
    }

    public void setPacotes(PacotesEntity pacotes) {
        this.pacotes = pacotes;
    }
}
