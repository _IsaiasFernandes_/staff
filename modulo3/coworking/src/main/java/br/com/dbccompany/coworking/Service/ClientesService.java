package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ClientesDTO;
import br.com.dbccompany.coworking.Entity.ClientesEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Repository.ClientesRepository;
import br.com.dbccompany.coworking.Repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClientesService extends ServiceAbstract<ClientesRepository, ClientesEntity, Integer>{

    @Autowired
    ContatoRepository contatoRepository;

    public ClientesDTO salvarComContatos( ClientesDTO cliente ) {
        List<ContatoEntity> contato = new ArrayList<>();
        contato.add(validacaoContato("email"));
        contato.add(validacaoContato("telefone"));

        cliente.setContato(contato);
        ClientesEntity clienteFinal = cliente.convert();
        ClientesDTO newDTO = new ClientesDTO(repository.save(clienteFinal));
        return newDTO;
    }

    private ContatoEntity validacaoContato( String nome ) {

        ContatoEntity contato = contatoRepository.findByValor(nome);
        contato = (contato == null) ? contatoRepository.save(new ContatoEntity()) : contato ;
        return contato;
    }

}
