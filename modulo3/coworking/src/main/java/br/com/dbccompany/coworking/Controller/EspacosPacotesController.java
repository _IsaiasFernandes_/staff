package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.EspacosPacotesDTO;
import br.com.dbccompany.coworking.Entity.EspacosPacotesEntity;
import br.com.dbccompany.coworking.Service.EspacosPacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/espacos_pacotes")
public class EspacosPacotesController {

    @Autowired
    EspacosPacotesService service;


    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public EspacosPacotesDTO Especifico(@PathVariable Integer id){
        EspacosPacotesDTO espacosPacotesDTO = new EspacosPacotesDTO(service.porId(id));
        return espacosPacotesDTO;
    }

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<EspacosPacotesDTO> todos() {
        List<EspacosPacotesDTO> espacosPacotesDTOS= new ArrayList<>();
        for(EspacosPacotesEntity acesso : service.todos()){
            espacosPacotesDTOS.add(new EspacosPacotesDTO(acesso));
        }
        return espacosPacotesDTOS;
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public EspacosPacotesDTO salvar(@RequestBody EspacosPacotesDTO entity){
        EspacosPacotesEntity espacosPacotesEntity = entity.convert();
        EspacosPacotesDTO espacoPacoteDTO = new EspacosPacotesDTO(service.salvar(espacosPacotesEntity));
        return espacoPacoteDTO;
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public EspacosPacotesDTO editar(@PathVariable Integer id, @RequestBody EspacosPacotesDTO entity){
        EspacosPacotesEntity espacosPacotesEntity = entity.convert();
        EspacosPacotesDTO espacosPacotesDTO = new EspacosPacotesDTO(service.editar(espacosPacotesEntity,id));
        return espacosPacotesDTO;
    }
}
