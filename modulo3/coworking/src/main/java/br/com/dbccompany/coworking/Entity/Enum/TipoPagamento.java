package br.com.dbccompany.coworking.Entity.Enum;

public enum TipoPagamento {
    DEBITO, CREDITO, DINHEIRO, TRANSFERENCIA
}
