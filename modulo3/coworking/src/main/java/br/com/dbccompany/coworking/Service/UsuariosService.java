package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.UsuariosDTO;
import br.com.dbccompany.coworking.Entity.UsuariosEntity;
import br.com.dbccompany.coworking.Repository.UsuariosRepository;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class UsuariosService extends ServiceAbstract<UsuariosRepository, UsuariosEntity, Integer>{


    public List<UsuariosDTO> retornarListaUsuarios() {
        List<UsuariosDTO> listDTO = new ArrayList<>();
        for (UsuariosEntity usuario : this.todos()) {
            listDTO.add(new UsuariosDTO(usuario));
        }

        return listDTO;
    }

    public UsuariosEntity salvar(UsuariosEntity usuario){
        String senha = usuario.getPassword();
        if(senha.length() >= 6){
            usuario.setPassword(new BCryptPasswordEncoder().encode(usuario.getPassword()));
            return repository.save(usuario);
        }
        return null;
    }

    public boolean login( String username, String password ) {
        UsuariosEntity usuario = repository.findByUsernameAndPassword(username, new BCryptPasswordEncoder().encode(password));
        return usuario == null ? false : true;
    }
}
