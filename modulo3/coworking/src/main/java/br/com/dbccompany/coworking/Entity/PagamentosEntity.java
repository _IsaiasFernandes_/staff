package br.com.dbccompany.coworking.Entity;

import br.com.dbccompany.coworking.Entity.Enum.TipoPagamento;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

@Entity
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = PagamentosEntity.class)
public class PagamentosEntity extends EntityAbstract<Integer>{

    @Id
    @SequenceGenerator(name = "PAGAMENTOS_SEQ", sequenceName = "PAGAMENTOS_SEQ")
    @GeneratedValue( generator = "PAGAMENTOS_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private TipoPagamento tipoPagamento;

    @ManyToOne
    @JoinColumn(name = "ID_CLIENTE_PACOTE")
    private ClientesPacotesEntity clientePacote;

    @ManyToOne
    @JoinColumn(name = "ID_CONTRATACAO")
    private ContratacaoEntity contratacao;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoPagamento getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamento tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

    public ContratacaoEntity getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContratacaoEntity contratacao) {
        this.contratacao = contratacao;
    }

    public ClientesPacotesEntity getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(ClientesPacotesEntity clientePacote) {
        this.clientePacote = clientePacote;
    }
}
