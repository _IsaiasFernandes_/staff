package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ClientesPacotesDTO;
import br.com.dbccompany.coworking.Entity.ClientesPacotesEntity;
import br.com.dbccompany.coworking.Service.ClientesPacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/clientes_pacotes")
public class ClientesPacotesController{

    @Autowired
    ClientesPacotesService service;


    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public ClientesPacotesDTO Especifico(@PathVariable Integer id){
        ClientesPacotesDTO clientesPacotesDTO = new ClientesPacotesDTO(service.porId(id));
        return clientesPacotesDTO;
    }

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<ClientesPacotesDTO> todos() {
        List<ClientesPacotesDTO> ClientesPacotesDTOS= new ArrayList<>();
        for(ClientesPacotesEntity acesso : service.todos()){
            ClientesPacotesDTOS.add(new ClientesPacotesDTO(acesso));
        }
        return ClientesPacotesDTOS;
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public ClientesPacotesDTO salvar(@RequestBody ClientesPacotesDTO entity){
        ClientesPacotesEntity clientesPacotesEntity = entity.convert();
        ClientesPacotesDTO clientesPacotesDTO = new ClientesPacotesDTO(service.salvar(clientesPacotesEntity));
        return clientesPacotesDTO;
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public ClientesPacotesDTO editar(@PathVariable Integer id, @RequestBody ClientesPacotesDTO entity){
        ClientesPacotesEntity clientesPacotesEntity = entity.convert();
        ClientesPacotesDTO clientesPacotesDTO = new ClientesPacotesDTO(service.editar(clientesPacotesEntity,id));
        return clientesPacotesDTO;
    }
}
