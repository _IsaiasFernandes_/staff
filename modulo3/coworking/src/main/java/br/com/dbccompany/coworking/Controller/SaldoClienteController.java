package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.SaldoClienteDTO;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntityId;
import br.com.dbccompany.coworking.Service.SaldoClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/saldo_cliente")
public class SaldoClienteController {

    @Autowired
    SaldoClienteService service;


    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<SaldoClienteDTO> todos() {
        List<SaldoClienteDTO> saldoClienteDTOS= new ArrayList<>();
        for(SaldoClienteEntity saldo : service.todos()){
            saldoClienteDTOS.add(new SaldoClienteDTO(saldo));
        }
        return saldoClienteDTOS;
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public SaldoClienteDTO salvar(@RequestBody SaldoClienteDTO entity){
        SaldoClienteEntity saldoClienteEntity = entity.convert();
        SaldoClienteDTO saldoClienteDTO = new SaldoClienteDTO(service.salvar(saldoClienteEntity));
        return saldoClienteDTO;
    }

    @GetMapping(value = "/ver/{cliente}/{espaco}")
    @ResponseBody
    public SaldoClienteDTO saldoClienteEspecifico(@PathVariable Integer cliente, @PathVariable Integer espaco) {
        Optional<SaldoClienteEntity> optionalEntity = service.porId(cliente, espaco);
        SaldoClienteEntity saldoClienteEntity = optionalEntity.isPresent() ? optionalEntity.get() : null;
        SaldoClienteDTO saldoClienteDTO = new SaldoClienteDTO(saldoClienteEntity);
        return saldoClienteDTO;
    }

    @PutMapping(value = "/editar/{cliente}/{espaco}")
    @ResponseBody
    public SaldoClienteDTO editarSaldoCliente(@PathVariable Integer cliente, @PathVariable Integer espaco, @RequestBody SaldoClienteDTO saldoClienteDTO) {
        SaldoClienteEntity saldoCliente = saldoClienteDTO.convert();
        SaldoClienteDTO newDTO = new SaldoClienteDTO(service.editar(saldoCliente, cliente, espaco));
        return newDTO;
    }
}
