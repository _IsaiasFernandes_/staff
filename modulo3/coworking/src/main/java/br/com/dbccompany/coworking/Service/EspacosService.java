package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.EspacosDTO;
import br.com.dbccompany.coworking.Entity.EspacosEntity;
import br.com.dbccompany.coworking.Repository.EspacosRepository;
import org.springframework.stereotype.Service;

@Service
public class EspacosService extends ServiceAbstract<EspacosRepository, EspacosEntity, Integer>{
}
