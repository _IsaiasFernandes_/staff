package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.PacotesEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.sql.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class PacotesRepositoryTest {

    @Autowired
    private PacotesRepository repository;

    @Test
    public void salvarPacoteBuscarPorId(){
        PacotesEntity pacote = new PacotesEntity();
        pacote.setValor(2000);

        repository.save(pacote);
        assertEquals(1, repository.findById(1).get().getId());
    }

    @Test
    public void buscarPacotePorValor(){
        PacotesEntity pacote = new PacotesEntity();
        pacote.setValor(2.0);


        repository.save(pacote);
        assertEquals(pacote.getValor(), repository.findByValor(2.0).getValor());
    }

    @Test
    public void buscarPacotePorValorQueNaoExiste(){
        PacotesEntity pacote = new PacotesEntity();
        pacote.setValor(2.0);

        repository.save(pacote);
        assertNull( repository.findByValor(3));
    }

    @Test
    public void buscarPacotePorIdQueNaoExiste(){

        assertEquals(Optional.empty(),repository.findById(15));
    }
}
