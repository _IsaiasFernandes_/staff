package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.ClientesEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Repository.ClientesRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class ClientesControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ClientesRepository repository;


    @Test
    public void deveRetornar200QuandoConsultadoCliente() throws Exception {
        URI uri = new URI("/api/clientes/todos");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void salvarERetornarUmCLiente() throws Exception {
        URI uri = new URI("/api/clientes/novo");
        String json = "{" +
                "\"nome\": \"Almir\",\n" +
                "\"cpf\": \"11234567891\",\n" +
                "\"dataNascimento\": \"10/10/1995\",\n" +
                "\"contato\": {\"id\": 1}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome").value("Almir")
        );
    }

    @Test
    public void deveRetornarUmCliente() throws Exception {
        ClientesEntity cliente = new ClientesEntity();
        List<ContatoEntity> contatos = new ArrayList<>();

        cliente.setNome("Tuta");
        cliente.setCpf("12345678934");
        cliente.setContato(contatos);
        cliente.setDataNascimento(Date.valueOf("1993-10-10"));
        // mapper.writerWithDefaultPrettyPrinter().

        ClientesEntity newCliente = repository.save(cliente);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/clientes/ver/{id}", newCliente.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome").value("Tuta")
        );
    }
}
