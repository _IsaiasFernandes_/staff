package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.AcessosEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntityId;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.sql.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

@DataJpaTest
public class AcessosRepositoryTest {

    @Autowired
    private AcessosRepository repository;

    @Test
    public void salvarAcesso(){

        SaldoClienteEntityId id = new SaldoClienteEntityId(1, 1);

        SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
        saldoCliente.setId(id);

        AcessosEntity acesso = new AcessosEntity();
        acesso.setSaldoCliente(saldoCliente);
        acesso.setEntrada(true);
        acesso.setExcecao(false);
        acesso.setData(new Date(1995, 10, 10));

        repository.save(acesso);

        assertEquals(1, repository.findById(1).get().getId());
    }

    @Test
    public void buscarAcessosInexistentes(){
        assertFalse(repository.findById(1).isPresent());
    }
}
