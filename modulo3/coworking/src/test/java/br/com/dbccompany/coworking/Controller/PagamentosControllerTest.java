package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.PagamentosEntity;
import br.com.dbccompany.coworking.Repository.PagamentosRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class PagamentosControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PagamentosRepository repository;


    @Test
    public void deveRetornar200QuandoConsultadoPagamento() throws Exception {
        URI uri = new URI("/api/pagamentos/todos");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void salvarERetornarUmPagamento() throws Exception {
        URI uri = new URI("/api/pagamentos/novo");
        String json = "{\"nome\": \"Brasil\"}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome").value("Brasil")
        );
    }

    @Test
    public void deveRetornarUmPagamento() throws Exception {
        PagamentosEntity pagamentos = new PagamentosEntity();

        PagamentosEntity newPagamentos = repository.save(pagamentos);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/pagamentos/ver/{id}", newPagamentos.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome").value("")
        );
    }
}
