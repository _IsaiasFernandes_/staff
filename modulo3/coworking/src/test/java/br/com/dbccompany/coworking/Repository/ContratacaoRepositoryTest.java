package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClientesEntity;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.Enum.TipoContratacao;
import br.com.dbccompany.coworking.Entity.EspacosEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.sql.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class ContratacaoRepositoryTest {

    @Autowired
    ContratacaoRepository repository;
    @Autowired
    ClientesRepository clientesRepository;
    @Autowired
    EspacosRepository espacosRepository;

    @Test
    public void salvarContratacao() {

        ClientesEntity cliente = new ClientesEntity();
        EspacosEntity espaco = new EspacosEntity();

        ContratacaoEntity contratacao = new ContratacaoEntity();

        clientesRepository.save(cliente);
        espacosRepository.save(espaco);

        contratacao.setEspaco(espaco);
        contratacao.setCliente(cliente);
        contratacao.setTipoContratacao(TipoContratacao.HORA);
        contratacao.setQuantidade(5);
        contratacao.setDesconto(1);
        contratacao.setPrazo(2);

        repository.save(contratacao);

        assertEquals(1, repository.findById(1).get().getId());

    }

    @Test
    public void buscarContratacao(){
        assertEquals(Optional.empty(),repository.findById(11));
        assertTrue(repository.findAllByTipoContratacao(TipoContratacao.SEMANA).isEmpty());
    }


}
