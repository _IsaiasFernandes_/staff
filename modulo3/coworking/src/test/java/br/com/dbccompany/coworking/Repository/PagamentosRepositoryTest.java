package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Entity.Enum.TipoContratacao;
import br.com.dbccompany.coworking.Entity.Enum.TipoPagamento;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.sql.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

@DataJpaTest
public class PagamentosRepositoryTest {

    @Autowired
    PagamentosRepository repository;

    @Autowired
    ContratacaoRepository contratacaoRepository;

    @Autowired
    ClientesPacotesRepository clientePacoteRepository;

    @Autowired
    ClientesRepository clienteRepository;

    @Autowired
    EspacosRepository espacoRepository;

    @Autowired
    PacotesRepository pacoteRepository;


    @Test
    public void buscarPagamentoPorTipo(){
        //Pacote
        PacotesEntity pacote = new PacotesEntity();
        pacote.setValor(12);

        //Cliente
        ClientesEntity cliente = new ClientesEntity();
        cliente.setNome("blabla");
        cliente.setCpf("12365478999");
        cliente.setDataNascimento(new Date(1992,10,10));

        ClientesEntity newCliente = clienteRepository.save(cliente);

        //Espaco
        EspacosEntity espaco = new EspacosEntity();
        espaco.setNome("adasdas0");
        espaco.setQtdPessoas(10);
        espaco.setValor(100);

        //ClientePacote
        ClientesPacotesEntity cliPacote = new ClientesPacotesEntity();
        cliPacote.setCliente(newCliente);
        cliPacote.setPacote(pacoteRepository.save(pacote));
        cliPacote.setQuantidade(2);

        //Contratacao
        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setCliente(newCliente);
        contratacao.setEspaco(espacoRepository.save(espaco));
        contratacao.setTipoContratacao(TipoContratacao.SEMANA);
        contratacao.setQuantidade(5);
        contratacao.setPrazo(30);

        PagamentosEntity pagamento = new PagamentosEntity();
        pagamento.setClientePacote(clientePacoteRepository.save(cliPacote));
        pagamento.setContratacao(contratacaoRepository.save(contratacao));
        pagamento.setTipoPagamento(TipoPagamento.CREDITO);
        repository.save(pagamento);

        assertEquals(1, repository.findAllByTipoPagamento(TipoPagamento.CREDITO).size());
    }

    @Test
    public void salvarPagamentoBuscarPorId(){
        PagamentosEntity pagamento = new PagamentosEntity();

        ClientesPacotesEntity cliPacote = new ClientesPacotesEntity();
        ContratacaoEntity contratacao = new ContratacaoEntity();

        clientePacoteRepository.save(cliPacote);
        contratacaoRepository.save(contratacao);

        pagamento.setClientePacote(cliPacote);
        pagamento.setContratacao(contratacao);
        pagamento.setTipoPagamento(TipoPagamento.CREDITO);

        repository.save(pagamento);

        assertEquals(1, repository.findById(1).get().getId());
    }

    @Test
    public void buscarPagamentoInexistente(){
        assertFalse(repository.findById(2).isPresent());
    }

}
