package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
public class TipoContatoRepositoryTest {

    @Autowired
    private TipoContatoRepository repository;

    @Test
    public void salvarTipoContato(){
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("email");

        repository.save(tipoContato);
        assertEquals(tipoContato.getNome(), repository.findByNome("email").getNome());
    }

    @Test
    public void buscarTipoContatoNaoExistente(){
        String nome = "email";
        assertNull(repository.findByNome(nome));
    }

    @Test
    public void retornarTodos(){

        TipoContatoEntity email = new TipoContatoEntity();
        TipoContatoEntity telefone = new TipoContatoEntity();

        List<TipoContatoEntity> contatos = new ArrayList<>();

        contatos.add(email);
        contatos.add(telefone);

        email.setNome("email");
        telefone.setNome("telefone");

        repository.save(email);
        repository.save(telefone);

        assertEquals(contatos, repository.findAll());
    }

    @Test
    public void editarNome(){
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("email");

        repository.save(tipoContato);
        assertEquals("email", repository.findByNome("email").getNome());

        tipoContato.setNome("e-mail");
        repository.save(tipoContato);

        assertEquals("e-mail", repository.findByNome("e-mail").getNome());

    }
}
