package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.EspacosPacotesEntity;
import br.com.dbccompany.coworking.Repository.EspacosPacotesRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class EspacosPacotesControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private EspacosPacotesRepository repository;


    @Test
    public void deveRetornar200QuandoConsultadoEspacosPacotes() throws Exception {
        URI uri = new URI("/api/espacos_pacotes/todos");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void salvarERetornarUmEspacosPacotes() throws Exception {
        URI uri = new URI("/api/espacos_pacotes/novo");
        String json = "{\"nome\": \"Brasil\"}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome").value("Brasil")
        );
    }

    @Test
    public void deveRetornarUmEspacosPacotes() throws Exception {
        EspacosPacotesEntity espacosPacotes = new EspacosPacotesEntity();

        EspacosPacotesEntity newespacosP = repository.save(espacosPacotes);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/espacos_pacotes/ver/{id}", newespacosP.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome").value("")
        );
    }
}
