package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClientesEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class ClientesRepositoryTest {

    @Autowired
    private ClientesRepository repository;

    @Test
    public void salvarClienteBuscarPorId() {
        ClientesEntity cliente = new ClientesEntity();
        List<ContatoEntity> contatos = new ArrayList<>();

        cliente.setNome("Tito");
        cliente.setCpf("12345678998");
        cliente.setContato(contatos);
        cliente.setDataNascimento(new Date(1992,10,10));

        repository.save(cliente);

        assertEquals(1, repository.findById(1).get().getId());
    }

    @Test
    public void findByCPF() {
        ClientesEntity cliente = new ClientesEntity();
        cliente.setNome("Cuubluegs");
        cliente.setCpf("1234567890");
        cliente.setDataNascimento(new Date(06,06,1954));

        repository.save(cliente);
        assertEquals(cliente, repository.findByCpf("1234567890"));
    }

    @Test
    public void buscarClientePorNomeECpf(){
        ClientesEntity cliente = new ClientesEntity();
        List<ContatoEntity> contatos = new ArrayList<>();

        cliente.setNome("Fravinhu");
        cliente.setCpf("1234567890");
        cliente.setContato(contatos);
        cliente.setDataNascimento(Date.valueOf("1987-10-10"));

        repository.save(cliente);

        assertEquals("1234567890", repository.findByNomeAndCpf("Fravinhu", "1234567890").getCpf());
    }


}
