package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.UsuariosEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class UsuariosRepositoryTest {

    @Autowired
    UsuariosRepository repository;

    @Test
    public void salvarUsuario(){
        UsuariosEntity usuario = new UsuariosEntity();

        usuario.setNome("teste");
        usuario.setEmail("teste@gmail.com");
        usuario.setUsername("teste");
        usuario.setPassword("123456");

        repository.save(usuario);
        assertEquals(usuario.getNome(),
                repository.findByNome("teste").get().getNome()
        );
    }

    @Test
    public void findByUsername() {
        UsuariosEntity usuarios = new UsuariosEntity();
        usuarios.setNome("blabos");
        usuarios.setEmail("blabos@gmail");
        usuarios.setPassword("abc123");
        usuarios.setUsername("blabos");

        repository.save(usuarios);

        assertEquals(usuarios, repository.findByNome("blabos").get());
    }

    @Test
    public void buscarUsuarioNaoExistente(){
        String nome = "blabos";

        assertEquals(Optional.empty() ,repository.findByNome(nome));
    }

    @Test
    public void buscarPorLogin(){
        UsuariosEntity usuario = new UsuariosEntity();

        usuario.setNome("blabos");
        usuario.setEmail("blabos@gmail.com");
        usuario.setUsername("blabos");
        usuario.setPassword("123456");

        repository.save(usuario);
        assertEquals("blabos",
                repository.findByNome("blabos").get().getNome()
        );
    }
}
