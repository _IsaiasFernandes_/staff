package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.EspacosEntity;
import br.com.dbccompany.coworking.Repository.EspacosRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class EspacosControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private EspacosRepository repository;


    @Test
    public void deveRetornar200QuandoConsultadoEspacos() throws Exception {
        URI uri = new URI("/api/espacos/todos");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void salvarERetornarUmEspacos() throws Exception {
        URI uri = new URI("/api/espacos/novo");
        String json = "{\"nome\": \"Brasil\"}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome").value("Brasil")
        );
    }

    @Test
    public void deveRetornarUmEspacos() throws Exception {
        EspacosEntity espaco = new EspacosEntity();
        espaco.setNome("Argentina");

        EspacosEntity newEspaco = repository.save(espaco);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/espacos/ver/{id}", espaco.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome").value("Argentina")
        );
    }
}
