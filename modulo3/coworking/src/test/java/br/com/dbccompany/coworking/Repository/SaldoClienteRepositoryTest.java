package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClientesEntity;
import br.com.dbccompany.coworking.Entity.Enum.TipoContratacao;
import br.com.dbccompany.coworking.Entity.EspacosEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntityId;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
public class SaldoClienteRepositoryTest {

    @Autowired
    SaldoClienteRepository repository;
    @Autowired
    EspacosRepository espacoRep;
    @Autowired
    ClientesRepository clienteRep;

    @Test
    public void salvarSaldoClienteBuscarPorId(){

        SaldoClienteEntityId id = new SaldoClienteEntityId(1, 1);

        ClientesEntity cliente = new ClientesEntity();
        EspacosEntity espaco = new EspacosEntity();
        espacoRep.save(espaco);
        clienteRep.save(cliente);

        SaldoClienteEntity saldoCli = new SaldoClienteEntity();

        saldoCli.setId(id);
        saldoCli.setCliente(cliente);
        saldoCli.setEspaco(espaco);
        saldoCli.setTipoContratacao(TipoContratacao.HORA);
        saldoCli.setQuantidade(3);
        saldoCli.setVencimento(LocalDate.now().plusDays(30));

        repository.save(saldoCli);

        assertEquals(1, repository.findById(id).get().getId().getCliente());
    }

    @Test
    public void buscarSaldoClientesInexistentes(){
        SaldoClienteEntityId id = new SaldoClienteEntityId(1, 1);
        assertEquals(Optional.empty(),repository.findById(id));
    }
}
