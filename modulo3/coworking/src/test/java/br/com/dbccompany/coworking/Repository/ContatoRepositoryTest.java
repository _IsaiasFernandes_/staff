package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClientesEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.sql.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
public class ContatoRepositoryTest {

    @Autowired
    ContatoRepository contatoRepository;

    @Autowired
    TipoContatoRepository tipoContatoRepository;

    @Autowired
    ClientesRepository clientesRepository;

    @Test
    public void salvarContato(){

        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("email");
        tipoContatoRepository.save(tipoContato);

        ContatoEntity contato = new ContatoEntity();
        contato.setTipoContato(tipoContato);
        contato.setValor("marlin@gmail.com");

        contatoRepository.save(contato);

        assertEquals(1, contatoRepository.findById(1).get().getId());
    }

    @Test
    public void findByValor() {
        ContatoEntity contato = new ContatoEntity();
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        ClientesEntity cliente = new ClientesEntity();

        cliente.setNome("Cuubluegs");
        cliente.setCpf("1234567890");
        cliente.setDataNascimento(new Date(06,06,1954));

        tipoContato.setNome("email");
        contato.setValor("2000");
        contatoRepository.save(contato);

        assertEquals(contato, contatoRepository.findByValor(contato.getValor()));
    }

    @Test
    public void buscarContatoNaoExistente(){

        String valor = "teste";
        assertNull(contatoRepository.findByValor(valor));
    }
}
