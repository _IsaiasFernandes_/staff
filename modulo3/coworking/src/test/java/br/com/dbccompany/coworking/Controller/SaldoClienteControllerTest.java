package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.Repository.SaldoClienteRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class SaldoClienteControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private SaldoClienteRepository repository;


    @Test
    public void deveRetornar200QuandoConsultadoSaldoCliente() throws Exception {
        URI uri = new URI("/api/saldo_cliente/todos");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void salvarERetornarUmSaldoCliente() throws Exception {
        URI uri = new URI("/api/saldo_cliente/novo");
        String json = "{\"nome\": \"Brasil\"}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome").value("Brasil")
        );
    }

    @Test
    public void deveRetornarUmSaldoCliente() throws Exception {
        SaldoClienteEntity saldoCliente = new SaldoClienteEntity();

        SaldoClienteEntity newSaldoC = repository.save(saldoCliente);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/saldo_cliente/ver/{id}", newSaldoC.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome").value("")
        );
    }
}
