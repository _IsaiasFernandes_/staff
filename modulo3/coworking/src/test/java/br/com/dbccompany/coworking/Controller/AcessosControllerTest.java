package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Entity.Enum.TipoContratacao;
import br.com.dbccompany.coworking.Repository.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class AcessosControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    @Autowired
    private AcessosRepository repository;

    @Autowired
    private ClientesRepository clientesR;

    @Autowired
    private ContatoRepository contatoR;

    @Autowired
    private EspacosRepository espacosR;

    @Autowired
    private SaldoClienteRepository saldoClienteR;

    @Autowired
    private TipoContatoRepository tipoContatoR;


    @Test
    public void salvarERetornarUmAcesso() throws Exception {

        ObjectMapper mapper = new ObjectMapper();

        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("email");
        TipoContatoEntity newTipoContato = tipoContatoR.save(tipoContato);
        ContatoEntity contato = new ContatoEntity();
        contato.setTipoContato(newTipoContato);
        contato.setValor("teste@gmail.com");
        ContatoEntity contato1 = contatoR.save(contato);

        ClientesEntity cliente = new ClientesEntity();
        cliente.setNome("teste");
        cliente.setCpf("01987654321");
        cliente.setDataNascimento(new Date(1990, 10, 12));
        List<ContatoEntity> listContatos = new ArrayList<>();
        listContatos.add(contato1);
        cliente.setContato(listContatos);
        ClientesEntity clienteEntity = clientesR.save(cliente);
        int idCliente = clienteEntity.getId();

        EspacosEntity espaco = new EspacosEntity();
        espaco.setNome("Loja 2");
        espaco.setQtdPessoas(10);
        espaco.setValor(300.00);
        EspacosEntity newEspaco = espacosR.save(espaco);
        int idEspaco = newEspaco.getId();

        SaldoClienteEntityId saldoClienteId = new SaldoClienteEntityId();
        saldoClienteId.setCliente(idCliente);
        saldoClienteId.setEspaco(idEspaco);

        SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
        saldoCliente.setId(saldoClienteId);
        saldoCliente.setVencimento(LocalDate.now().plusDays(30));
        saldoCliente.setQuantidade(10);
        saldoCliente.setTipoContratacao(TipoContratacao.DIARIA);
        SaldoClienteEntity newSaldoCliente = saldoClienteR.save(saldoCliente);

        AcessosEntity acessosEntity  = new AcessosEntity();
        acessosEntity.setExcecao(true);
        acessosEntity.setSaldoCliente(newSaldoCliente);

        String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(acessosEntity);

        URI uri = new URI("/api/acessos/novo");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.excecao").value(true)
        );
    }

    @Test
    public void deveRetornar200QuandoConsultadoAcesso() throws Exception {
        URI uri = new URI("/api/acessos/todos");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void deveRetornarUmAcesso() throws Exception {
        AcessosEntity acesso = new AcessosEntity();

        AcessosEntity newAcesso = repository.save(acesso);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/acessos/ver/{id}", newAcesso.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome").value("Argentina")
        );
    }
}
