package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Enum.TipoContratacao;
import br.com.dbccompany.coworking.Entity.EspacosEntity;
import br.com.dbccompany.coworking.Entity.EspacosPacotesEntity;
import br.com.dbccompany.coworking.Entity.PacotesEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.sql.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class EspacosPacotesRepositoryTest {

    @Autowired
    EspacosRepository espacoRep;
    @Autowired
    PacotesRepository pacoteRep;
    @Autowired
    EspacosPacotesRepository repository;

    @Test
    public void salvarEspacosPacotesBuscarPorId(){
        EspacosPacotesEntity espacosPacotes = new EspacosPacotesEntity();
        EspacosEntity espaco = new EspacosEntity();
        PacotesEntity pacotes = new PacotesEntity();

        espacoRep.save(espaco);
        pacoteRep.save(pacotes);

        espacosPacotes.setEspaco(espaco);
        espacosPacotes.setPacote(pacotes);
        espacosPacotes.setTipoContratacao(TipoContratacao.HORA);
        espacosPacotes.setQuantidade(5);
        espacosPacotes.setPrazo(3);

        repository.save(espacosPacotes);

        assertEquals(1, repository.findById(1).get().getId());

    }

    @Test
    public void buscarEspacosPacotesInexistentes(){
        assertEquals(Optional.empty(),repository.findById(2));
    }
}
