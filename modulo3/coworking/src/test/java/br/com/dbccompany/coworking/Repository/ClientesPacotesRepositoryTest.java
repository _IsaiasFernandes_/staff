package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Entity.Enum.TipoContratacao;
import br.com.dbccompany.coworking.Entity.Enum.TipoPagamento;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.sql.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
public class ClientesPacotesRepositoryTest {

    @Autowired
    ClientesPacotesRepository repository;
    @Autowired
    ClientesRepository clientesRepository;
    @Autowired
    PacotesRepository pacotesRepository;

    @Test
    public void salvarClientesPacotesBuscarPorId(){
        ClientesEntity cliente = new ClientesEntity();
        PacotesEntity pacotes = new PacotesEntity();

        clientesRepository.save(cliente);
        pacotesRepository.save(pacotes);

        ClientesPacotesEntity cliPacote = new ClientesPacotesEntity();

        cliPacote.setCliente(cliente);
        cliPacote.setPacote(pacotes);
        cliPacote.setQuantidade(5);

        repository.save(cliPacote);

        assertEquals(1, repository.findById(1).get().getId());
    }

    @Test
    public void buscarClientesPacotesInexistentes(){
        assertEquals(Optional.empty(),repository.findById(2));
    }
}
