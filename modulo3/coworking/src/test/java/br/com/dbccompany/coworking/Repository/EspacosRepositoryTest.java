package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.EspacosEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.sql.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class EspacosRepositoryTest {

    @Autowired
    EspacosRepository repository;

    @Test
    public void salvarEspacosBuscarPorId(){

        EspacosEntity espaco = new EspacosEntity();
        espaco.setNome("Sala 1");
        espaco.setQtdPessoas(10);
        espaco.setValor(100.0);

        repository.save(espaco);

        assertEquals(1, repository.findById(1).get().getId());
    }

    @Test
    public void buscarEspacosInexistente() {
        assertNull(repository.findByNome("Sala 2"));
        assertNull(repository.findByQtdPessoasAndValor( 10,45.0));
        assertEquals(Optional.empty(), repository.findById(10));
    }

    @Test
    public void buscarEspacoPorQtdPessoasEValor(){
        EspacosEntity espaco = new EspacosEntity();
        espaco.setNome("Sala 1");
        espaco.setQtdPessoas(10);
        espaco.setValor(100.0);

        repository.save(espaco);

        assertEquals("Sala 1",repository.findByQtdPessoasAndValor(10,100.0).getNome());
    }

    @Test
    public void buscarEspacoPorNome(){
        EspacosEntity espaco = new EspacosEntity();
        espaco.setNome("Sala 3");
        espaco.setQtdPessoas(25);
        espaco.setValor(50.0);

        repository.save(espaco);

        assertEquals("Sala 3",repository.findByNome("Sala 3").getNome());
    }
}
