package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.UsuariosEntity;
import br.com.dbccompany.coworking.Repository.UsuariosRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class UsuariosControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UsuariosRepository repository;


    @Test
    public void deveRetornar200QuandoConsultadoUsuario() throws Exception {
        URI uri = new URI("/api/usuarios/todos");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void salvarERetornarUmUsuario() throws Exception {
        URI uri = new URI("/api/usuarios/novo");
        String json = "{\"nome\": \"Brasil\"}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome").value("Brasil")
        );
    }

    @Test
    public void deveRetornarUmUsuario() throws Exception {
        UsuariosEntity usuario = new UsuariosEntity();
        usuario.setNome("Argentina");

        UsuariosEntity newUsuarios = repository.save(usuario);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/usuarios/ver/{id}", newUsuarios.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome").value("Argentina")
        );
    }
}
