package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.PaisEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class PaisRepositoryTest {

    @Autowired
    private PaisRepository repository;

    @Test
    public void salvarPais() {
        PaisEntity pais = new PaisEntity();
        pais.setNome("Brasil");
        repository.save(pais);
        assertEquals(pais.getNome(), repository.findByNome("Brasil").getNome());
    }

    @Test
    public void buscarPais() {
        String nome = "Brasil";
        assertNull(repository.findByNome(nome));
    }
}
