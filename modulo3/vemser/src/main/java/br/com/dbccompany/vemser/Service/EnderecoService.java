package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.EnderecoEntity;
import br.com.dbccompany.vemser.Repository.EnderecoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EnderecoService extends ServiceAbstract<EnderecoRepository, EnderecoEntity, Integer>{

    @Autowired
    private EnderecoRepository repository;

    public EnderecoEntity findByNumeroAndCepAndLogradouro(int numero, char[] cep, String logradouro) {
        return repository.findByNumeroAndCepAndLogradouro(numero, cep, logradouro);
    }

    public List<EnderecoEntity> findAllByBairro(String bairro) { return repository.findAllByBairro(bairro); }

    public List<EnderecoEntity> findAllByCep(char[] cep) { return repository.findAllByCep(cep); }

    public List<EnderecoEntity> findAllByNumero(int numero) { return repository.findAllByNumero(numero); }

    public List<EnderecoEntity> findAllByLogradouro(String logradouro) { return repository.findAllByLogradouro(logradouro); }
}
