package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.EnderecoEntity;
import br.com.dbccompany.vemser.Entity.Enum.EstadoCivil;
import br.com.dbccompany.vemser.Entity.UsuarioEntity;
import br.com.dbccompany.vemser.Repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsuarioService extends ServiceAbstract<UsuarioRepository, UsuarioEntity, Integer>{

    @Autowired
    UsuarioRepository repository;

    public UsuarioEntity findByEndereco(EnderecoEntity endereco) {
        return repository.findByEndereco(endereco);
    }

    public UsuarioEntity findByCpf(char cpf) {
        return repository.findByCpf(cpf);
    }

    public List<UsuarioEntity> findAllByNome(String nome) {
        return repository.findAllByNome(nome);
    }

    public List<UsuarioEntity> findAllByEstadoCivil(EstadoCivil estadoCivil) {
        return repository.findAllByEstadoCivil(estadoCivil);
    }

    public List<UsuarioEntity> findAllBydataNascimento(char dataNascimento) {
        return repository.findAllBydataNascimento(dataNascimento);
    }

    public UsuarioEntity salvar(UsuarioEntity usuario) {
        usuario.setSenha(new BCryptPasswordEncoder().encode(usuario.getSenha()));
        return repository.save(usuario);
    }

}
