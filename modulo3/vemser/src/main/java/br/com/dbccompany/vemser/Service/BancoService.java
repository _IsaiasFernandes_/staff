package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.BancoEntity;
import br.com.dbccompany.vemser.Repository.BancoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BancoService extends ServiceAbstract<BancoRepository, BancoEntity, Integer> {

    @Autowired
    private BancoRepository repository;

    public BancoEntity findByCodigo(int codigo) { return repository.findByCodigo(codigo); }

    public BancoEntity findByNome(String nome) { return repository.findByNome(nome); }

    public List<BancoEntity> findAllByCodigo(int codigo ) { return repository.findAllByCodigo(codigo); }

    public List<BancoEntity> findAllByNome(String nome) { return repository.findAllByNome(nome); }
}
