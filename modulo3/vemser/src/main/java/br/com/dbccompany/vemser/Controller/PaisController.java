package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.DTO.PaisDTO;
import br.com.dbccompany.vemser.Entity.PaisEntity;
import br.com.dbccompany.vemser.Service.PaisService;
import br.com.dbccompany.vemser.VemserApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/pais")
public class PaisController {

    private Logger logger = LoggerFactory.getLogger(VemserApplication.class);

    @Autowired
    PaisService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<PaisEntity> todosPais() {
        logger.info("Começou a buscar os paises!");
        return service.todos();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public PaisDTO salvar(@RequestBody PaisDTO pais){
        PaisEntity paisEntity = pais.convert();
        PaisDTO newDto = new PaisDTO(service.salvar(paisEntity));
        return newDto;
    }

    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public PaisEntity paisEspecifico(@PathVariable Integer id){
        return service.porId(id);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public PaisEntity editarPais(@PathVariable Integer id, @RequestBody PaisEntity pais){
        return service.editar(pais, id);
    }
}
