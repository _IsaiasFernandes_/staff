package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.DTO.GeralDTO;
import br.com.dbccompany.vemser.Service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping( "/api/geral")
public class GeralController {

    @Autowired
    PaisService paisService;

    @Autowired
    EstadoService estadoService;

    @Autowired
    CidadeService cidadeService;

    @Autowired
    EnderecoService enderecoService;

    @Autowired
    AgenciaService agenciaService;

    @Autowired
    ContaService contaService;

    @PostMapping( value = "/salvaTudo" )
    @ResponseBody
    public GeralDTO salvaTudo(@RequestBody GeralDTO geral) {
        GeralDTO newGeral = new GeralDTO();
        newGeral.setPais(paisService.salvar(geral.getPais()));
        newGeral.setEstado(estadoService.salvar(geral.getEstado()));
        newGeral.setCidade(cidadeService.salvar(geral.getCidade()));
        newGeral.setEndereco(enderecoService.salvar(geral.getEndereco()));
        newGeral.setAgencia(agenciaService.salvar(geral.getAgencia()));
        newGeral.setConta(contaService.salvar(geral.getConta()));

        return newGeral;
    }

    @PostMapping( value = "/relatorio" )
    @ResponseBody
    public GeralDTO relatorio(@RequestBody GeralDTO geral) {
        GeralDTO newGeral = new GeralDTO();
        newGeral.setPais(paisService.porId(geral.getPais().getId()));
        newGeral.setEstado(estadoService.porId(geral.getEstado().getId()));
        newGeral.setCidade(cidadeService.porId(geral.getCidade().getId()));
        newGeral.setEndereco(enderecoService.porId(geral.getEndereco().getId()));
        newGeral.setAgencia(agenciaService.porId(geral.getAgencia().getId()));
        newGeral.setConta(contaService.porId(geral.getConta().getId()));

        return newGeral;
    }
}
