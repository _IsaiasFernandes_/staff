package br.com.dbccompany.vemser.Entity;

import br.com.dbccompany.vemser.Entity.Enum.TipoMovimentacao;

import javax.persistence.*;

@Entity
public class MovimentacoesEntity extends EntityAbstract<Integer> {

    @Id
    @SequenceGenerator(name = "MOVIMENTACOES_SEQ", sequenceName = "MOVIMENTACOES_SEQ")
    @GeneratedValue( generator = "MOVIMENTACOES_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private double valor;

    @Enumerated( EnumType.STRING)
    private TipoMovimentacao tipoMovimentacao;

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumns({
            @JoinColumn( name = "id_conta"),
            @JoinColumn( name = "id_tipo_conta")
    })
    private ContaEntity conta;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public ContaEntity getConta() {
        return conta;
    }

    public void setConta(ContaEntity conta) {
        this.conta = conta;
    }

    public TipoMovimentacao getTipoMovimentacao() {
        return tipoMovimentacao;
    }

    public void setTipoMovimentacao(TipoMovimentacao tipoMovimentacao) {
        this.tipoMovimentacao = tipoMovimentacao;
    }
}
