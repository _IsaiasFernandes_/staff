package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.ConsolidacaoEntity;
import br.com.dbccompany.vemser.Service.ConsolidacaoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/consolidacao")
public class ConsolidacaoController extends ControllerAbstract<ConsolidacaoService, ConsolidacaoEntity, Integer>{
}
