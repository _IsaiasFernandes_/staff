package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.TipoContaEntity;
import br.com.dbccompany.vemser.Service.TipoContaService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/tipo_conta")
public class TipoContaController extends ControllerAbstract<TipoContaService, TipoContaEntity, Integer>{
}
