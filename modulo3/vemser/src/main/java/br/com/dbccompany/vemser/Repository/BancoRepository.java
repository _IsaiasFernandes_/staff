package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.BancoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BancoRepository extends CrudRepository<BancoEntity, Integer> {

    BancoEntity findByCodigo(int codigo);
    BancoEntity findByNome(String nome);

    List<BancoEntity> findAllByCodigo(int codigo);
    List<BancoEntity> findAllByNome(String nome);
}
