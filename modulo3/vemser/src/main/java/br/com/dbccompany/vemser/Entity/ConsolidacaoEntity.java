package br.com.dbccompany.vemser.Entity;

import javax.persistence.*;

@Entity
public class ConsolidacaoEntity extends EntityAbstract<Integer> {

    @Id
    @SequenceGenerator(name = "CONSOLIDACAO_SEQ", sequenceName = "CONSOLIDACAO_SEQ")
    @GeneratedValue( generator = "CONSOLIDACAO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private double saldoAtual;
    private double saques;
    private double depositos;
    private int qtdCorrentistas;

    {
        saldoAtual = 0.0;
        saques = 0.0;
        depositos = 0.0;
        qtdCorrentistas = 0;
    }

    @OneToOne( fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_AGENCIA" )
    private AgenciaEntity agencia;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getSaldoAtual() {
        return saldoAtual;
    }

    public void setSaldoAtual(double saldoAtual) {
        this.saldoAtual = saldoAtual;
    }

    public double getSaques() {
        return saques;
    }

    public void setSaques(double saques) {
        this.saques = saques;
    }

    public double getDepositos() {
        return depositos;
    }

    public void setDepositos(double depositos) {
        this.depositos = depositos;
    }

    public int getQtdCorrentistas() {
        return qtdCorrentistas;
    }

    public void setQtdCorrentistas(int qtdCorrentistas) {
        this.qtdCorrentistas = qtdCorrentistas;
    }

    public AgenciaEntity getAgencia() {
        return agencia;
    }

    public void setAgencia(AgenciaEntity agencia) {
        this.agencia = agencia;
    }
}
