package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.EnderecoEntity;
import br.com.dbccompany.vemser.Entity.Enum.EstadoCivil;
import br.com.dbccompany.vemser.Entity.UsuarioEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UsuarioRepository extends CrudRepository<UsuarioEntity, Integer> {

    Optional<UsuarioEntity> findByLogin(String login);

    UsuarioEntity findByEndereco(EnderecoEntity endereco);
    UsuarioEntity findByCpf(char cpf);

    List<UsuarioEntity> findAllByNome(String nome);
    List<UsuarioEntity> findAllByEstadoCivil(EstadoCivil estadoCivil);
    List<UsuarioEntity> findAllBydataNascimento(char dataNascimento);
}
