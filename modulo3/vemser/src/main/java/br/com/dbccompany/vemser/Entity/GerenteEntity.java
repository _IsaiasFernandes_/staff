package br.com.dbccompany.vemser.Entity;

import br.com.dbccompany.vemser.Entity.Enum.TipoGerente;

import javax.persistence.*;
import java.util.List;

@Entity
@PrimaryKeyJoinColumn( name = "id")
public class GerenteEntity extends UsuarioEntity {

    private int codigoFuncionario;

    @Enumerated( EnumType.STRING)
    private TipoGerente tipoGerente;

    @ManyToMany( cascade = CascadeType.ALL )
    @JoinTable ( name = "CONTA_X_GERENTE",
            joinColumns = { @JoinColumn(name = "id_gerente")},
            inverseJoinColumns = { @JoinColumn( name = "id_conta"), @JoinColumn(name = "id_tipo_conta")}
    )
    private List<ContaEntity> contas;

    public int getCodigoFuncionario() {
        return codigoFuncionario;
    }

    public void setCodigoFuncionario(int codigoFuncionario) {
        this.codigoFuncionario = codigoFuncionario;
    }

    public TipoGerente getTipoGerente() {
        return tipoGerente;
    }

    public void setTipoGerente(TipoGerente tipoGerente) {
        this.tipoGerente = tipoGerente;
    }

    public List<ContaEntity> getContas() {
        return contas;
    }

    public void setContas(List<ContaEntity> contas) {
        this.contas = contas;
    }
}
