package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.CidadeEntity;
import br.com.dbccompany.vemser.Repository.CidadeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CidadeService extends ServiceAbstract<CidadeRepository, CidadeEntity, Integer>{

    @Autowired
    private CidadeRepository repository;

    public CidadeEntity buscarCidadePeloNome(String nome) {
        return repository.findByNome(nome);
    }

}
