package br.com.dbccompany.vemser.Entity;

import br.com.dbccompany.vemser.Entity.Enum.EstadoCivil;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.Collection;
import java.util.List;

@Entity
@Inheritance( strategy = InheritanceType.JOINED)
public class UsuarioEntity extends EntityAbstract<Integer> implements Serializable, UserDetails {

    @Id
    @SequenceGenerator(name = "USUARIO_SEQ", sequenceName = "USUARIO_SEQ")
    @GeneratedValue( generator = "USUARIO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private String nome;
    @Column( length = 11, columnDefinition = "CHAR")
    private String cpf;

    private String login;
    private String senha;

    @Enumerated( EnumType.STRING)
    private EstadoCivil estadoCivil;

    @JsonFormat( shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date dataNascimento;

    @OneToOne( fetch = FetchType.LAZY)
    @JoinColumn( name = "ID_ENDERECO")
    private EnderecoEntity endereco;

    @OneToMany(mappedBy = "usuario")
    private List<ContaClienteEntity> contaClientes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public EstadoCivil getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(EstadoCivil estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public EnderecoEntity getEndereco() {
        return endereco;
    }

    public void setEndereco(EnderecoEntity endereco) {
        this.endereco = endereco;
    }

    public List<ContaClienteEntity> getContaClientes() {
        return contaClientes;
    }

    public void setContaClientes(List<ContaClienteEntity> contaClientes) {
        this.contaClientes = contaClientes;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return this.senha;
    }

    @Override
    public String getUsername() {
        return this.login;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
