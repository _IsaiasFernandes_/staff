package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.DTO.PaisDTO;
import br.com.dbccompany.vemser.Entity.PaisEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaisRepository extends CrudRepository<PaisEntity, Integer> {

    PaisEntity findByNome(String nome);
    List<PaisEntity> findAllByNome(String nome);

    //PaisEntity findByIdAndNome( int id, String nome );
}
