package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.EstadoEntity;
import br.com.dbccompany.vemser.Repository.EstadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EstadoService extends ServiceAbstract<EstadoRepository, EstadoEntity, Integer>{

    @Autowired
    private EstadoRepository repository;

    public EstadoEntity buscarEstadoPeloNome(String nome) {
        return repository.findByNome(nome);
    }

}
