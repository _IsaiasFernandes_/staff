package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.EstadoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EstadoRepository extends CrudRepository<EstadoEntity, Integer> {

    EstadoEntity findByNome(String nome);
}
