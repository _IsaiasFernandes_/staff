package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.ContaEntity;
import br.com.dbccompany.vemser.Entity.ContaEntityId;
import br.com.dbccompany.vemser.Repository.ContaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContaService  extends ServiceAbstract<ContaRepository, ContaEntity, ContaEntityId>{

    @Autowired
    private ContaRepository repository;

    public ContaEntity findByCodigo(int codigo) { return repository.findByCodigo(codigo); }

    public List<ContaEntity> findAllByCodigo(int codigo) { return repository.findAllByCodigo(codigo); }

}
