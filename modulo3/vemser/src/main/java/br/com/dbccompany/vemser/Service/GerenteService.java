package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.Enum.TipoGerente;
import br.com.dbccompany.vemser.Entity.GerenteEntity;
import br.com.dbccompany.vemser.Repository.GerenteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class GerenteService extends ServiceAbstract<GerenteRepository, GerenteEntity, Integer>{

    @Autowired
    private GerenteRepository repository;

    public GerenteEntity findByCodigoFuncionario(int codigo) { return repository.findByCodigoFuncionario(codigo); }

    public GerenteEntity findByTipoGerente(TipoGerente tipo) { return  repository.findByTipoGerente(tipo); }

    public List<GerenteEntity> findAllByTipoGerente(TipoGerente tipo) { return repository.findAllByTipoGerente(tipo); }
}
