package br.com.dbccompany.vemser.Exception;

public class ArgumentoInvalidoBanco extends ExceptionBanco{

    public ArgumentoInvalidoBanco() {
        super("Faltou argumentos para o banco!");
    }

}
