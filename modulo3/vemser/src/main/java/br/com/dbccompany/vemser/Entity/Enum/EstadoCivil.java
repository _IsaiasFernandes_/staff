package br.com.dbccompany.vemser.Entity.Enum;

public enum EstadoCivil {
    CASADO,
    SOLTEIRO,
    DIVORCIADO,
    UNIAO_ESTAVEL,
    VIUVO
}
