package br.com.dbccompany.vemser.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class TipoContaEntity extends EntityAbstract<Integer> {

    @Id
    @SequenceGenerator(name = "TIPO_CONTA_SEQ", sequenceName = "TIPO_CONTA_SEQ")
    @GeneratedValue( generator = "TIPO_CONTA_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private String nome;

    @OneToMany(mappedBy = "tipoConta")
    private List<ContaEntity> conta;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<ContaEntity> getConta() {
        return conta;
    }

    public void setConta(List<ContaEntity> conta) {
        this.conta = conta;
    }
}
