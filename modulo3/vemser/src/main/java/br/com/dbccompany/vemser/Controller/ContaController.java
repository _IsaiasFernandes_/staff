package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.ContaEntity;
import br.com.dbccompany.vemser.Entity.ContaEntityId;
import br.com.dbccompany.vemser.Service.ContaService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/conta")
public class ContaController extends ControllerAbstract<ContaService, ContaEntity, ContaEntityId>{

}
