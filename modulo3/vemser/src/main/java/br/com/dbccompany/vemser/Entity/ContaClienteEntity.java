package br.com.dbccompany.vemser.Entity;

import javax.persistence.*;

@Entity
@Table( name = "CONTA_X_CLIENTE" )
public class ContaClienteEntity extends EntityAbstract<ContaClienteId>{

    @EmbeddedId
    private ContaClienteId id;

    @ManyToOne
    @MapsId("ID_USUARIO")
    @JoinColumn( name = "ID_USUARIO", nullable = false)
    private UsuarioEntity usuario;

    @ManyToOne
    @MapsId("ID_BANCO")
    @JoinColumn( name = "ID_BANCO", nullable = false)
    private BancoEntity banco;

    @ManyToOne
    @MapsId("ID_TIPO_CONTA")
    @JoinColumns({
            @JoinColumn(name = "ID_CONTA", nullable = false),
            @JoinColumn(name = "ID_TIPO_CONTA", nullable = false)
    })
    private ContaEntity conta;

    public ContaClienteId getId() {
        return id;
    }

    public void setId(ContaClienteId id) {
        this.id = id;
    }

    public UsuarioEntity getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioEntity usuario) {
        this.usuario = usuario;
    }

    public BancoEntity getBanco() {
        return banco;
    }

    public void setBanco(BancoEntity banco) {
        this.banco = banco;
    }

    public ContaEntity getConta() {
        return conta;
    }

    public void setConta(ContaEntity conta) {
        this.conta = conta;
    }
}
