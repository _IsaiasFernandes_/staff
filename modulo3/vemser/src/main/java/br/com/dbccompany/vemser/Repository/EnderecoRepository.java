package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.CidadeEntity;
import br.com.dbccompany.vemser.Entity.EnderecoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EnderecoRepository extends CrudRepository<EnderecoEntity, Integer> {

    EnderecoEntity findByNumeroAndCepAndLogradouro(int numero, char[] cep, String Logradouro);
    List<EnderecoEntity> findAllByBairro(String bairro);
    List<EnderecoEntity> findAllByCep(char[] cep);
    List<EnderecoEntity> findAllByNumero(int numero);
    List<EnderecoEntity> findAllByLogradouro(String logradouro);
}
