package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.EntityAbstract;
import br.com.dbccompany.vemser.Service.ServiceAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/")
public abstract class ControllerAbstract<S extends ServiceAbstract, E extends EntityAbstract, T>{

    @Autowired
    S service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<E> todos() {
        return service.todos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public E salvar(@RequestBody E entity){
        return (E) service.salvar(entity);
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public E espeficifico(@PathVariable T id) {
        return (E) service.porId(id);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public E editar(@PathVariable T id, @RequestBody E entity) {
        return (E) service.editar(entity, id);
    }
}
