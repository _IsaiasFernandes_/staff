package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.TipoContaEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TipoContaRepository extends CrudRepository<TipoContaEntity, Integer> {

    TipoContaEntity findByNome(String nome);
    List<TipoContaEntity> findAllByNome(String nome);
}
