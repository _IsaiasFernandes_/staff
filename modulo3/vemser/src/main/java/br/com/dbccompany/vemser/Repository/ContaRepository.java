package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.ContaEntity;
import br.com.dbccompany.vemser.Entity.ContaEntity;
import br.com.dbccompany.vemser.Entity.ContaEntityId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContaRepository extends CrudRepository<ContaEntity, ContaEntityId> {

    ContaEntity findByCodigo(int codigo);
    List<ContaEntity> findAllByCodigo(int codigo);
}
