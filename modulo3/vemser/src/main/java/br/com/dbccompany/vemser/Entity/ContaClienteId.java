package br.com.dbccompany.vemser.Entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class ContaClienteId implements Serializable {

    @Column( name = "ID_USUARIO")
    private int idUsuario;

    @Column( name = "ID_TIPO_CONTA")
    private int idTipoConta;

    @Column( name = "ID_BANCO")
    private int idBanco;

    public ContaClienteId() {
    }

    public ContaClienteId(int idUsuario, int idTipoConta, int idBanco) {
        this.idUsuario = idUsuario;
        this.idTipoConta = idTipoConta;
        this.idBanco = idBanco;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getIdTipoConta() {
        return idTipoConta;
    }

    public void setIdTipoConta(int idTipoConta) {
        this.idTipoConta = idTipoConta;
    }

    public int getIdBanco() {
        return idBanco;
    }

    public void setIdBanco(int idBanco) {
        this.idBanco = idBanco;
    }
}
