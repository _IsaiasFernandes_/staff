package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.AgenciaEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AgenciaRepository extends CrudRepository<AgenciaEntity, Integer> {

    AgenciaEntity findByCodigo(int codigo);
    AgenciaEntity findByNome(String nome);

    List<AgenciaEntity> findAllByCodigo(int codigo);
    List<AgenciaEntity> findAllByNome(String nome);
}
