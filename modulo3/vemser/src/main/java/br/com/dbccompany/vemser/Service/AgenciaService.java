package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.AgenciaEntity;
import br.com.dbccompany.vemser.Repository.AgenciaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AgenciaService extends ServiceAbstract<AgenciaRepository, AgenciaEntity, Integer>{

    @Autowired
    private AgenciaRepository repository;

    public AgenciaEntity findByCodigo(int codigo) { return repository.findByCodigo(codigo); }

    public AgenciaEntity findByNome(String nome) { return repository.findByNome(nome); }

    public List<AgenciaEntity> findAllByCodigo(int codigo ) { return repository.findAllByCodigo(codigo); }

    public List<AgenciaEntity> findAllByNome(String nome) { return repository.findAllByNome(nome); }
}
