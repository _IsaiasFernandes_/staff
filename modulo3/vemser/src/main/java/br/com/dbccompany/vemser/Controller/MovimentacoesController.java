package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.MovimentacoesEntity;
import br.com.dbccompany.vemser.Service.MovimentacoesService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/movimentacoes")
public class MovimentacoesController extends ControllerAbstract<MovimentacoesService, MovimentacoesEntity, Integer>{
}
