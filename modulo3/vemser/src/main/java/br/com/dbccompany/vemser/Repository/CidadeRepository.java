package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.CidadeEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CidadeRepository extends CrudRepository<CidadeEntity, Integer> {

    CidadeEntity findByNome(String nome);

}
