package br.com.dbccompany.vemser.Entity;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class ContaEntityId implements Serializable {

    @SequenceGenerator(name = "CONTA_SEQ", sequenceName = "CONTA_SEQ")
    @GeneratedValue( generator = "CONTA_SEQ", strategy = GenerationType.SEQUENCE)
    private int id;

    @Column( name = "ID_TIPO_CONTA")
    private int idTipoConta;

    public ContaEntityId() {}

    public ContaEntityId(int id, int idTipoConta) {
        this.id = id;
        this.idTipoConta = idTipoConta;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdTipoConta() {
        return idTipoConta;
    }

    public void setIdTipoConta(int idTipoConta) {
        this.idTipoConta = idTipoConta;
    }
}
