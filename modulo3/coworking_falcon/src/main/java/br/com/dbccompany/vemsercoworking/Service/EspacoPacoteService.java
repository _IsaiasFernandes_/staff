package br.com.dbccompany.vemsercoworking.Service;

import br.com.dbccompany.vemsercoworking.Entity.EspacoPacoteEntity;
import br.com.dbccompany.vemsercoworking.Repository.EspacoPacoteRepository;
import org.springframework.stereotype.Service;

@Service
public class EspacoPacoteService extends ServiceAbstract<EspacoPacoteRepository, EspacoPacoteEntity, Integer> {
}
