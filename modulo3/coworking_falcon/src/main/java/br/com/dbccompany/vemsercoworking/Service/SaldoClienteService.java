package br.com.dbccompany.vemsercoworking.Service;

import br.com.dbccompany.vemsercoworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.vemsercoworking.Entity.SaldoClienteId;
import br.com.dbccompany.vemsercoworking.Repository.SaldoClienteRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class SaldoClienteService extends ServiceAbstract<SaldoClienteRepository, SaldoClienteEntity, SaldoClienteId> {
    @Transactional(rollbackFor = Exception.class)
    public SaldoClienteEntity editar(SaldoClienteEntity entidade, Integer idCliente, Integer idEspaco) {
        SaldoClienteId newId = new SaldoClienteId(idCliente, idEspaco);
        entidade.setId(newId);

        return repository.save(entidade);
    }

    @Transactional(rollbackFor = Exception.class)
    public Optional<SaldoClienteEntity> porId(Integer idCliente, Integer idEspaco) {
        SaldoClienteId newId = new SaldoClienteId(idCliente, idEspaco);
        Optional<SaldoClienteEntity> optionalEntity = repository.findById(newId);
        return optionalEntity;
    }
}
