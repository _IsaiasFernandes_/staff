package br.com.dbccompany.vemsercoworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class TipoContatoEntity extends EntityAbstract<Integer> {
    @Id
    @SequenceGenerator(name = "TIPO_CONTATO_SEQ", sequenceName = "TIPO_CONTATO_SEQ")
    @GeneratedValue(generator = "TIPO_CONTATO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(nullable = false, unique = true)
    private String nome;

    @OneToMany (mappedBy = "tipoContato")
    private List<ContatoEntity> contatos;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<ContatoEntity> getContatos() {
        return contatos;
    }

    public void setContatos(List<ContatoEntity> contatos) {
        this.contatos = contatos;
    }
}
