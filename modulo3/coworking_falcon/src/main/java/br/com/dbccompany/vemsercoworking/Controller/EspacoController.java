package br.com.dbccompany.vemsercoworking.Controller;

import br.com.dbccompany.vemsercoworking.DTO.EspacoDTO;
import br.com.dbccompany.vemsercoworking.Entity.EspacoEntity;
import br.com.dbccompany.vemsercoworking.Service.EspacoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/espaco")
public class EspacoController {

    @Autowired
    EspacoService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<EspacoDTO> todosEspaco() {
        List<EspacoDTO> listaDTO = new ArrayList<>();
        for (EspacoEntity espaco : service.todos()) {
            listaDTO.add(new EspacoDTO(espaco));
        }
        return listaDTO;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public EspacoDTO salvar(@RequestBody EspacoDTO espacoDTO){
        EspacoEntity espacoEntity = espacoDTO.convert();
        EspacoDTO newDto = new EspacoDTO(service.salvar(espacoEntity));
        return newDto;
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public EspacoDTO espacoEspecifico(@PathVariable Integer id) {
        EspacoDTO espacoDTO = new EspacoDTO(service.porId(id));
        return espacoDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public EspacoDTO editarEspaco(@PathVariable Integer id, @RequestBody EspacoDTO espacoDTO) {
        EspacoEntity espaco = espacoDTO.convert();
        EspacoDTO newDTO = new EspacoDTO(service.editar(espaco, id));
        return newDTO;
    }
}
