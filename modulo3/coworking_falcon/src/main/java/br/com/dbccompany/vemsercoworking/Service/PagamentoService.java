package br.com.dbccompany.vemsercoworking.Service;

import br.com.dbccompany.vemsercoworking.Entity.PagamentoEntity;
import br.com.dbccompany.vemsercoworking.Repository.PagamentoRepository;
import org.springframework.stereotype.Service;

@Service
public class PagamentoService extends ServiceAbstract<PagamentoRepository, PagamentoEntity, Integer> {
}
