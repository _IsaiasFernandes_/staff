package br.com.dbccompany.vemsercoworking.Util;

import br.com.dbccompany.vemsercoworking.Entity.TipoContratacaoEnum;

public class ConvertePrazoParaDiasUtil {
    public static double converteParaDias(int quantidade, TipoContratacaoEnum tipoContratacaoEnum)  {
        double qtdDias = 0;
        switch (tipoContratacaoEnum) {
            case MES:
                qtdDias = 30.0;
                break;
            case SEMANA:
                qtdDias = 7.0;
                break;
            case DIARIA:
                qtdDias = 1.0;
                break;
            case TURNO:
                qtdDias = 0.33;
                break;
            case HORA:
                qtdDias = 0.042;
                break;
            case MINUTO:
                qtdDias = 0.00007;
                break;
        }
        return (double) quantidade * qtdDias;
    }
}
