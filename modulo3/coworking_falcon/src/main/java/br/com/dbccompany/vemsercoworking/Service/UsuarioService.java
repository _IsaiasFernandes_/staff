package br.com.dbccompany.vemsercoworking.Service;

import br.com.dbccompany.vemsercoworking.DTO.UsuarioDTO;
import br.com.dbccompany.vemsercoworking.Entity.UsuarioEntity;
import br.com.dbccompany.vemsercoworking.Repository.UsuarioRepository;
import br.com.dbccompany.vemsercoworking.Util.CriptografiaMD5Util;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UsuarioService extends ServiceAbstract<UsuarioRepository, UsuarioEntity, Integer> {

    public List<UsuarioDTO> retornarListaUsuarios() {
        //Stream -> Java Funcional (lambda)
        List<UsuarioDTO> listaDTO = new ArrayList<>();
        for (UsuarioEntity usuario : this.todos()) {
            listaDTO.add(new UsuarioDTO(usuario));
        }

        return listaDTO;
    }

    //123 -> dghsgdjkashdjhasjdhashd232423dmnasdjk
    //dghsgdjkashdjhasjdhashd232423dmnasdjk -> 123

    //dghsgdjkashdjhasjdhashd232423dmnasdjk == 123

    public boolean login( String login, String senha ) {
        UsuarioEntity usuario = repository.findByLoginAndSenha(login, CriptografiaMD5Util.criptografaSenha(senha));
        return usuario == null ? false : true;
    }

}
