package br.com.dbccompany.vemsercoworking.DTO;

import br.com.dbccompany.vemsercoworking.Entity.ClienteEntity;
import br.com.dbccompany.vemsercoworking.Entity.ContatoEntity;
import br.com.dbccompany.vemsercoworking.Entity.TipoContatoEntity;

import java.util.List;

public class ContatoDTO {

    private Integer id;
    private TipoContatoEntity tipoContato;
    private ClienteEntity cliente;
    private String valor;

    public ContatoDTO() {}

    public ContatoDTO(ContatoEntity contato) {
        this.id = contato.getId();
        this.cliente = contato.getCliente();
        this.tipoContato = contato.getTipoContato();
        this.valor = contato.getValor();
    }

    public ContatoEntity convert() {
        ContatoEntity contato = new ContatoEntity();
        contato.setId(this.id);
        contato.setTipoContato(this.tipoContato);
        contato.setCliente(this.cliente);
        contato.setValor(this.valor);
        return contato;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContatoEntity getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContatoEntity tipoContato) {
        this.tipoContato = tipoContato;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
