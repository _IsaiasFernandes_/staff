package br.com.dbccompany.vemsercoworking.Repository;

import br.com.dbccompany.vemsercoworking.Entity.AcessoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AcessoRepository extends CrudRepository<AcessoEntity, Integer>{
}
