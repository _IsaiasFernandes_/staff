package br.com.dbccompany.vemsercoworking.Service;

import br.com.dbccompany.vemsercoworking.Entity.TipoContatoEntity;
import br.com.dbccompany.vemsercoworking.Repository.TipoContatoRepository;
import org.springframework.stereotype.Service;

@Service
public class TipoContatoService extends ServiceAbstract<TipoContatoRepository, TipoContatoEntity, Integer> {
}
