package br.com.dbccompany.vemsercoworking.Controller;

import br.com.dbccompany.vemsercoworking.DTO.ContatoDTO;
import br.com.dbccompany.vemsercoworking.Entity.ContatoEntity;
import br.com.dbccompany.vemsercoworking.Service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/contato")
public class ContatoController {

    @Autowired
    ContatoService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<ContatoDTO> todosContato() {
        List<ContatoDTO> listaDTO = new ArrayList<>();
        for (ContatoEntity contato : service.todos()) {
            listaDTO.add(new ContatoDTO(contato));
        }
        return listaDTO;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public ContatoDTO salvar(@RequestBody ContatoDTO contatoDTO){
        ContatoEntity contatoEntity = contatoDTO.convert();
        ContatoDTO newDto = new ContatoDTO(service.salvar(contatoEntity));
        return newDto;
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public ContatoDTO contatoEspecifico(@PathVariable Integer id) {
        ContatoDTO contatoDTO = new ContatoDTO(service.porId(id));
        return contatoDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ContatoDTO editarContato(@PathVariable Integer id, @RequestBody ContatoDTO contatoDTO) {
        ContatoEntity contato = contatoDTO.convert();
        ContatoDTO newDTO = new ContatoDTO(service.editar(contato, id));
        return newDTO;
    }
}
