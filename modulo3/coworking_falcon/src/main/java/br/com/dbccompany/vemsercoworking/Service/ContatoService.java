package br.com.dbccompany.vemsercoworking.Service;

import br.com.dbccompany.vemsercoworking.Entity.ContatoEntity;
import br.com.dbccompany.vemsercoworking.Repository.ContatoRepository;
import org.springframework.stereotype.Service;

@Service
public class ContatoService extends ServiceAbstract<ContatoRepository, ContatoEntity, Integer> {
}
