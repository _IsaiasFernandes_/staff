package br.com.dbccompany.vemsercoworking.DTO;

import br.com.dbccompany.vemsercoworking.Entity.AcessoEntity;
import br.com.dbccompany.vemsercoworking.Entity.SaldoClienteEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.time.LocalDate;
import java.time.Period;

public class AcessoDTO {
    private Integer id;
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private SaldoClienteEntity saldoCliente;
    private boolean isEntrada;
    private LocalDate data;
    private boolean isExcecao;

    public AcessoDTO() {}

    public AcessoDTO(AcessoEntity acesso) {
        this.id = acesso.getId();
        this.saldoCliente = acesso.getSaldoCliente();
        this.isEntrada = acesso.isEntrada();
        this.data = acesso.getData();
        this.isExcecao = acesso.isExcecao();
        if(isEntrada){
            boolean dataVencimento = this.saldoCliente.getVencimento().compareTo(LocalDate.now()) > 0;
            if(!dataVencimento || saldoCliente.getQuantidade() <= 0){
                System.out.println("Saldo Insuficiente");
            } else {
                Period period = Period.between(this.saldoCliente.getVencimento(), LocalDate.now());
                System.out.println("Ainda restam " + period.getDays() + " dias.");
            }
        }
    }

    public AcessoEntity convert() {
        AcessoEntity acesso = new AcessoEntity();
        acesso.setId(this.id);
        acesso.setSaldoCliente(this.saldoCliente);
        acesso.setEntrada(this.isEntrada);
        acesso.setData(this.data);
        acesso.setExcecao(this.isExcecao);
        return acesso;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SaldoClienteEntity getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoClienteEntity saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public boolean isEntrada() {
        return isEntrada;
    }

    public void setEntrada(boolean entrada) {
        isEntrada = entrada;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public boolean isExcecao() {
        return isExcecao;
    }

    public void setExcecao(boolean excecao) {
        isExcecao = excecao;
    }
}
