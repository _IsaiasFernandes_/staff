package br.com.dbccompany.vemsercoworking.DTO;

import br.com.dbccompany.vemsercoworking.Entity.*;

import java.util.List;

public class ContratacaoDTO {
    private Integer id;
    private ClienteEntity cliente;
    private EspacoEntity espaco;
    private TipoContratacaoEnum tipoContratacao;
    private int quantidade;
    private double desconto = 0.0;
    private int prazo;
    private List<PagamentoEntity> pagamentos;
    private double valorContratado;

    public ContratacaoDTO() {}

    public ContratacaoDTO(ContratacaoEntity contratacao) {
        this.id = contratacao.getId();
        this.cliente = contratacao.getCliente();
        this.espaco = contratacao.getEspaco();
        this.tipoContratacao = contratacao.getTipoContratacao();
        this.quantidade = contratacao.getQuantidade();
        this.desconto = contratacao.getDesconto();
        this.prazo = contratacao.getPrazo();
        this.pagamentos = contratacao.getPagamentos();
        this.valorContratado = contratacao.getEspaco().getValor() - this.desconto;
    }

    public ContratacaoEntity convert() {
        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setId(this.id);
        contratacao.setCliente(this.cliente);
        contratacao.setEspaco(this.espaco);
        contratacao.setTipoContratacao(this.tipoContratacao);
        contratacao.setQuantidade(this.quantidade);
        contratacao.setDesconto(this.desconto);
        contratacao.setPrazo(this.prazo);
        contratacao.setPagamentos(this.pagamentos);
        return contratacao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public double getDesconto() {
        return desconto;
    }

    public void setDesconto(double desconto) {
        this.desconto = desconto;
    }

    public int getPrazo() {
        return prazo;
    }

    public void setPrazo(int prazo) {
        this.prazo = prazo;
    }

    public List<PagamentoEntity> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<PagamentoEntity> pagamentos) {
        this.pagamentos = pagamentos;
    }

    public double getValorContratado() {
        return valorContratado;
    }

    public void setValorContratado(double valorCobrado) {
        this.valorContratado = valorCobrado;
    }
}
