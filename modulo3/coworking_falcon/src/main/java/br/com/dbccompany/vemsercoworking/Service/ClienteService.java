package br.com.dbccompany.vemsercoworking.Service;

import br.com.dbccompany.vemsercoworking.DTO.ClienteDTO;
import br.com.dbccompany.vemsercoworking.DTO.ContatoDTO;
import br.com.dbccompany.vemsercoworking.Entity.ClienteEntity;
import br.com.dbccompany.vemsercoworking.Entity.ContatoEntity;
import br.com.dbccompany.vemsercoworking.Repository.ClienteRepository;
import br.com.dbccompany.vemsercoworking.Repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClienteService extends ServiceAbstract<ClienteRepository, ClienteEntity, Integer> {

    @Autowired
    ContatoRepository contatoRepository;

    public ClienteDTO salvarComContatos( ClienteDTO cliente ) {
        List<ContatoEntity> contato = new ArrayList<>();
        contato.add(validacaoContato("email"));
        contato.add(validacaoContato("telefone"));

        cliente.setContatos(contato);
        ClienteEntity clienteFinal = cliente.convert();
        ClienteDTO newDTO = new ClienteDTO(repository.save(clienteFinal));
        return newDTO;
    }

    private ContatoEntity validacaoContato( String nome ) {
        /*ContatoEntity contato = contatoRepository.findByValor(nome);
        if( contato == null ){
            contato = contatoRepository.save(new ContatoEntity());
        }
        return contato;*/

        ContatoEntity contato = contatoRepository.findByValor(nome);
        contato = (contato == null) ? contatoRepository.save(new ContatoEntity()) : contato ;
        return contato;
    }

}
