package br.com.dbccompany.vemsercoworking.Controller;

import br.com.dbccompany.vemsercoworking.DTO.PagamentoDTO;
import br.com.dbccompany.vemsercoworking.Entity.PagamentoEntity;
import br.com.dbccompany.vemsercoworking.Service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/pagamento")
public class PagamentoController {

    @Autowired
    PagamentoService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<PagamentoDTO> todosPagamento() {
        List<PagamentoDTO> listaDTO = new ArrayList<>();
        for (PagamentoEntity pagamento : service.todos()) {
            listaDTO.add(new PagamentoDTO(pagamento));
        }
        return listaDTO;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public PagamentoDTO salvar(@RequestBody PagamentoDTO pagamentoDTO){
        PagamentoEntity pagamentoEntity = pagamentoDTO.convert();
        PagamentoDTO newDto = new PagamentoDTO(service.salvar(pagamentoEntity));
        return newDto;
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public PagamentoDTO pagamentoEspecifico(@PathVariable Integer id) {
        PagamentoDTO pagamentoDTO = new PagamentoDTO(service.porId(id));
        return pagamentoDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public PagamentoDTO editarPagamento(@PathVariable Integer id, @RequestBody PagamentoDTO pagamentoDTO) {
        PagamentoEntity pagamento = pagamentoDTO.convert();
        PagamentoDTO newDTO = new PagamentoDTO(service.editar(pagamento, id));
        return newDTO;
    }
}
