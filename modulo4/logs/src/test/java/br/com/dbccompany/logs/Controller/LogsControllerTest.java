package br.com.dbccompany.logs.Controller;

import br.com.dbccompany.logs.Entity.LogsEntity;
import br.com.dbccompany.logs.Repository.LogsRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
public class LogsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private LogsRepository repository;


    @Test
    public void deveRetornar200QuandoConsultadoLogs () throws  Exception {
        URI uri = new URI("/api/logs/todos");

        mockMvc
                .perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status()
                        .is(200)
                );
    }

    @Test
    public void salvarRetornarUmLog() throws Exception {
        URI uri = new URI("/api/logs/novo");
        String json = "{\"id\": \"11111111111\"," +
                "    \"codigo\": \"22222222\"," +
                "    \"mensagem\": \"teste\"}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.codigo").value("22222222")
        );
    }

    @Test
    public void deveRetornarUmLog() throws Exception {
        LogsEntity logsEntity = new LogsEntity();
        logsEntity.setId("11111111111");
        logsEntity.setCodigo("22222222222");
        logsEntity.setMessage("Novo teste");
        LogsEntity logsEntity1 = repository.save(logsEntity);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/logs/ver/{id}", logsEntity1.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.codigo")
                .value("22222222222")
        );
    }
}
