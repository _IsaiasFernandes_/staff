package br.com.dbccompany.logs.Repository;

import br.com.dbccompany.logs.Entity.LogsEntity;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;


@DataMongoTest
public class LogsRepositoryTest {

    @Autowired
    private LogsRepository repository;

    @Test
    public void findById() {
        LogsEntity logsEntity = new LogsEntity();
        logsEntity.setId("11111111111");
        logsEntity.setCodigo("22222222222");
        logsEntity.setMessage("Novo teste");
        repository.save(logsEntity);

        assertEquals("22222222222", repository.findById("11111111111").getCodigo());
    }

    @Test
    public void findBy2Ids() {
        LogsEntity log1 = new LogsEntity();
        log1.setId("5f6e3c6265c8591b17c2130d");
        log1.setCodigo("22222222222");
        log1.setMessage("Novo teste");
        repository.save(log1);

        LogsEntity log2 = new LogsEntity();
        log2.setId("5f6e3c6265c8591b17c2131c");
        log2.setCodigo("33333333");
        log2.setMessage("teste");
        repository.save(log2);

        assertEquals("5f6e3c6265c8591b17c2130d", repository.save(log1).getId());
        assertEquals("5f6e3c6265c8591b17c2131c", repository.save(log2).getId());
    }
}
