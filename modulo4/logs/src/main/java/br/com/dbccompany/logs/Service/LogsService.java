package br.com.dbccompany.logs.Service;

import br.com.dbccompany.logs.Entity.LogsEntity;
import br.com.dbccompany.logs.LogsApplication;
import br.com.dbccompany.logs.Repository.LogsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class LogsService {

    private Logger logger = LoggerFactory.getLogger(LogsApplication.class);

    @Autowired
    LogsRepository repository;

    @Transactional
    public LogsEntity salvar(LogsEntity entidade ) {
        try {
            return repository.save(entidade);
        }catch (Exception e) {
            logger.error("Erro ao salvar mensagemm: " + e.getMessage());
            return null;
        }
    }

    @Transactional
    public List<LogsEntity> todos() {
        return repository.findAll();
    }

    public LogsEntity porId( String id ) {
        return repository.findById(id);
    }
}
