package br.com.dbccompany.logs.Repository;

import br.com.dbccompany.logs.Entity.LogsEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogsRepository extends MongoRepository<LogsEntity, Integer> {

    LogsEntity findById(String id);
}
