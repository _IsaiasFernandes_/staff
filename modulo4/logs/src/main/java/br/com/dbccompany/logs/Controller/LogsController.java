package br.com.dbccompany.logs.Controller;

import br.com.dbccompany.logs.Entity.LogsEntity;
import br.com.dbccompany.logs.LogsApplication;
import br.com.dbccompany.logs.Service.LogsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/logs")
public class LogsController {

    private Logger logger = LoggerFactory.getLogger(LogsApplication.class);

    @Autowired
    LogsService service;


    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<LogsEntity> todosLogs() {
        return service.todos();
    }


    @PostMapping( value = "/novo" )
    @ResponseBody
    public LogsEntity salvar(@RequestBody LogsEntity logs){
        return service.salvar(logs);
    }

    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public LogsEntity logEspecifico(@PathVariable String id){
        return service.porId(id);
    }


}
