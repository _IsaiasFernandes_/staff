const arrayIds = [];

function verificaId( id ) {
  const array = window.localStorage.getItem( 'id' );
  for ( let i = 0; i < arrayIds.length; i++ ) {
    if ( arrayIds[i] === id ) {
      return true;
    }
  }
  return false;
}

function procuraUltimoId() {
  const array = window.localStorage.getItem( 'id' );
  return array[array.length - 1];
}

function funcaoAcharPokemon( idPokemon ) {
  const renderizar = ( pokemon ) => {
    const picture = document.getElementById( 'picture' );
    const dadosPokemon = document.getElementById( 'stats' );

    const id = dadosPokemon.querySelector( '.id' );
    id.innerHTML = pokemon.id;

    const nome = dadosPokemon.querySelector( '.nome' );
    nome.innerHTML = pokemon.nome;

    const imgPokemon = picture.querySelector( '.thumb' );
    imgPokemon.src = pokemon.imagem;

    const altura = dadosPokemon.querySelector( '.altura' );
    altura.innerHTML = pokemon.altura;

    const peso = dadosPokemon.querySelector( '.peso' );
    peso.innerHTML = pokemon.peso;

    const tipos = dadosPokemon.querySelector( '.tipos' );
    tipos.innerHTML = pokemon.tipos;

    const habilidades = dadosPokemon.querySelector( '.habilidades' );
    habilidades.innerHTML = pokemon.habilidades;

    const status = dadosPokemon.querySelector( '.status' );
    status.innerHTML = pokemon.status;
  }

  const pokeApi = new PokeApi();
  pokeApi
    .buscarEspecifico( idPokemon )
    .then( pokemon => {
      const poke = new Pokemon( pokemon );
      renderizar( poke );
    } );
}

function armazenarNoLocalStorage( id ) {
  arrayIds.push( id );
  window.localStorage.setItem( 'id', arrayIds );
  console.log( arrayIds );
}

function procurarPokemon() {
  const idInput = document.getElementById( 'idInput' ).value;
  if ( idInput === procuraUltimoId() ) {
    const customAlert = alert;
    customAlert( 'Numero já foi digitado' );
  } else {
    funcaoAcharPokemon( idInput );
    armazenarNoLocalStorage( idInput );
  }
}

function sortearPokemon() {
  const id = Math.floor( Math.random() * 893 + 1 );
  if ( verificaId( id ) );
  else {
    funcaoAcharPokemon( id );
    armazenarNoLocalStorage( id );
  }
}

const btnProcurar = document.getElementById( 'procurarPokemon' );
const btnSortear = document.getElementById( 'sortearPokemon' );

btnProcurar.onclick = procurarPokemon;
btnSortear.onclick = sortearPokemon;
