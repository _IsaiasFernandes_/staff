class PokeApi { // eslint-disable-line no-unused-vars
  constructor() {
    this._api = 'https://pokeapi.co/api/v2/pokemon';
  }

  buscarEspecifico( id ) {
    const fazRequisicao = fetch( `${ this._api }/${ id }` );
    return fazRequisicao.then( resultadoString => resultadoString.json() );
  }
}
