class Pokemon { // eslint-disable-line no-unused-vars
  constructor( objDaApi ) {
    this._id = objDaApi.id;
    this._nome = objDaApi.name;
    this._imagem = objDaApi.sprites.front_default;
    this._altura = objDaApi.height;
    this._peso = objDaApi.weight;
    this._tipos = objDaApi.types.map( type => type.type.name );
    this._habilidades = objDaApi.abilities.map( hab => hab.ability.name );
    this._status = objDaApi.stats.map( stat => `${ stat.stat.name }: ${ stat.base_stat }` );
  }

  get id() {
    return this._id;
  }

  get nome() {
    return this._nome;
  }

  get imagem() {
    return this._imagem;
  }

  get altura() {
    return `${ this._altura * 10 } cm.`;
  }

  get peso() {
    const kilo = ( this._peso / 2.2046 );
    return `${ kilo.toFixed( 2 ) } kg.`
  }

  get tipos() {
    const tipos = this._tipos.join( ', ' );
    return tipos;
  }

  get habilidades() {
    const habilidades = this._habilidades.join( ', ' );
    return habilidades;
  }

  get status() {
    const status = this._status.join( ', ' );
    return status;
  }
}
