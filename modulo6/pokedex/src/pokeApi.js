class PokeApi {

  constructor() {
    this._api = `https://pokeapi.co/api/v2/pokemon`;
  }
  buscarTodos() {
    let fazRequisicao = fetch(`${this._api}?limit=1050&offset=0`);
    return fazRequisicao.then(resultadoString => resultadoString.json());
  }

  buscarEspecifico(id) {
    let fazRequisicao = fetch(`${this._api}/${id}`);
    return fazRequisicao.then(resultadoString => resultadoString.json());
  }


}