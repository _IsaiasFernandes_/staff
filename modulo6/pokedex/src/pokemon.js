class Pokemon {

  constructor(objDaApi) {
    this._nome = objDaApi.name;
    this._imagem = objDaApi.sprites.front_default;
    this._altura = objDaApi.height;
    this._peso = objDaApi.weight;
    this._tipos = objDaApi.types.map(type => type.type.name);
  }

  get nome() {
    return this._nome;
  }

  get imagem() {
    return this._imagem;
  }

  get altura() {
    return `${this._altura * 10} cm.`;
  }

  get peso() {
    let kilo = (this._peso * 0.453592);
    return `${kilo.toFixed(2)} kg.`
  }

  get tipos() {
    let tipos = this._tipos.join(", ");
    return tipos;
  }
}