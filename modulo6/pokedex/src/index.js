let pokeApi = new PokeApi();
pokeApi
  .buscarEspecifico(112)
  .then(pokemon => {
    let poke = new Pokemon(pokemon);
    console.log(pokemon);
    renderizar(poke);
  }
  );

renderizar = (pokemon) => {
  let dadosPokemon = document.getElementById('dadosPokemon');
  let nome = dadosPokemon.querySelector(".nome");
  nome.innerHTML = pokemon.nome;

  let imgPokemon = dadosPokemon.querySelector('.thumb');
  imgPokemon.src = pokemon.imagem;

  let altura = dadosPokemon.querySelector(".altura");
  altura.innerHTML = pokemon.altura;

  let peso = dadosPokemon.querySelector(".peso");
  peso.innerHTML = pokemon.peso;

  let tipos = dadosPokemon.querySelector(".tipos");
  tipos.innerHTML = pokemon.tipos;
}