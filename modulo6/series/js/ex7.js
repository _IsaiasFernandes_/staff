//exercicio 07\\



Array.prototype.creditos = (serie) => {
  
  let titulo = serie.titulo;
  let diretores = serie.diretor.map( diretor => diretor);
  let elenco = serie.elenco.map( elenco => elenco);

  return `
    ${ titulo }
    ${ '\n' }
    Diretores:
    ${ '\n' }
    ${ diretores.reverse().sort().join( '\n' ) }
    ${ '\n' }
    Elenco:
    ${ '\n' }
    ${ elenco.reverse().sort().join( '\n' ) }  
    ${ '\n' }
  `
}

console.log( series.creditos( series[0] ) );
