//exercicio 06-A

Array.prototype.queroGenero = function(genero) {
  const generos = this.filter( serie => serie.genero.includes( genero ));
  const titulo = generos.map( genero => genero.titulo );

  return titulo;
}

console.log(`Série por Genero: ${ series.queroGenero( 'Drama' ) }` );

//exercicio 06-B

Array.prototype.queroTitulo = ( titulo ) => {
  const titulos = this.filter( serie => serie.titulo.includes( titulo ));
  const titulosSeries = titulos.map( titulo => titulo.titulo );

  return titulosSeries;
}

console.log(`Série por Título: ${ series.queroTitulo( 'Game' ) }` );
