//exercicio 01\\

const campoNull = serie => {
  return Object.values( serie ).some( valor => valor === null || typeof campo === 'undefined');
  /* const valores = Object.values( serie );
  for ( let valor in valores ) {
    if( !valor ){
      return serie;
    }
  } */
}

const vazio = vazio => { return vazio != null};

Array.prototype.invalidas = function() {
  let data = new Date;
  let ano = data.getFullYear();
  let titulosSeries = this.map( serie => {
    if( campoNull(serie) || serie.anoEstreia > ano ) {
      return serie.titulo;
    }
  })

  titulosSeries = titulosSeries.filter( vazio );
  
  return `Séries Inválidas: ${titulosSeries}`;
}

console.log( series.invalidas());