//exercicio 04

Array.prototype.mediaDeEpisodios = () => {
  let totalEp = 0;
  for(let i = 0; i < this.length; i++) {
    totalEp += this[i].numeroEpisodios !== undefined ? this[i].numeroEpisodios : 0;
  }
  return Math.ceil( totalEp / this.length );
}

console.log(`Media de episódos: ${ series.mediaDeEpisodios() }` );
