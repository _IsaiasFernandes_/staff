import React, { Component } from 'react';
import ListaEpisodios from '../../models/listaEpisodios';
import EpisodiosApi from '../../api/episodiosApi';
import './Home.css';

import {EpisodioUi, BotaoUi, MensagemFlash, MeuInputNumero } from '../../components';
import Mensagens from '../../constants/mensagens';

export default class Home extends Component {
  constructor( props ) {
    super( props );
    this.episodiosApi = new EpisodiosApi();
    this.marcarComoAssistido = this.marcarComoAssistido.bind( this );
    this.registrarNota = this.registrarNota.bind( this );
    this.state = {
      deveExibirMensagem: false,
      deveExibirErro: false,
      mensagem: ''
    };
  }

  componentDidMount() {
    const requisicoes = [
      this.episodiosApi.buscar(),
      this.episodiosApi.buscarTodasNotas()
    ];

    Promise.all( requisicoes )
      .then( respostas => {
        const episodiosDoServidor = respostas[ 0 ];
        const notasServidor = respostas[ 1 ];
        this.listaEpisodios = new ListaEpisodios( episodiosDoServidor, notasServidor );
        this.setState( state => { return { ...state, episodio: this.listaEpisodios.episodiosAleatorios } } );
      })
  }

  sortear() {
    const episodio = this.listaEpisodios.episodiosAleatorios;
    this.setState((state) => {
      return { ...state, episodio }
    })
  }

  marcarComoAssistido() {
    const { episodio } = this.state;
    episodio.marcarComoAssistido();
    this.setState( state => {
      return { ...state, episodio }
    })
  }

  registrarNota( { nota, erro } ) {
    this.setState( state => {
      return {
        ...state,
        deveExibirErro: erro
      }
    });
    if( erro ) {
      return;
    }

    const { episodio } = this.state;
    let corMensagem, mensagem;
    
    if (episodio.validarNota( nota )) {
      episodio.avaliar( nota ).then( () => {
        corMensagem = 'verde';
        mensagem = Mensagens.SUCESSO.REGISTRO_NOTA;
        this.exibirMensagem( { corMensagem, mensagem } );
      });
    }else{
      corMensagem = 'vermelho';
      mensagem = Mensagens.ERRO.NOTA_INVALIDA;
      this.exibirMensagem( { corMensagem, mensagem } );
    }
  }

  exibirMensagem = ({ corMensagem, mensagem }) => {
    this.setState( state => {
      return {
        ...state,
        deveExibirMensagem: true,
        mensagem,
        corMensagem
      }
    });
  }

  atualizarMensagem = devoExibir => {
    this.setState( state => {
      return { 
        ...state,
        deveExibirMensagem: devoExibir
      }
    });
  }

  linha( item, i ) {
    return <BotaoUi key={ i } classe={ item.cor } nome={ item.nome } metodo={ item.metodo } />
  }

  render() {
    const { episodio, deveExibirMensagem, mensagem, corMensagem, deveExibirErro } = this.state;
    const { listaEpisodios } = this;

    return (!episodio ? (
      <h3> Aguarde...</h3>
    ) : (
        <div className="App">
          <div className="botoes">
          <BotaoUi link={{ pathname: './avaliacoes', state: { listaEpisodios } }} classe={`botao`} nome={`Lista de avaliacoes`} />
          <BotaoUi link={{ pathname: './episodios', state: { listaEpisodios } }} classe={`botao`} nome={`Todos os episódios`} />
          </div>
          <MensagemFlash
            atualizarMensagem={this.atualizarMensagem}
            mensagem={mensagem}
            cor={corMensagem}
            exibir={deveExibirMensagem}
            segundos="5" />
          <header className="App-header">
            < EpisodioUi episodio={episodio} />
            <div className="spans">
              <span> Já assisti: {episodio.assistido ? 'Sim' : 'Não'}</span>
              <span> Assistiu {episodio.qtdVezesAssistido} vezes</span>
            </div>
            <div className="botoes">
              <BotaoUi classe={`botao`} nome={`Próximo`} metodo={this.sortear} />
              <BotaoUi classe={`botao`} nome={`Já assisti!`} metodo={this.marcarComoAssistido} />
            </div>
            <div className="nota">
              <span><strong>Nota: </strong>{episodio.nota}</span>
              <div className="descricao-nota">
                <MeuInputNumero
                  placeholder="Nota de 1 a 5"
                  mensagem="Qual a sua nota para esse episódio?"
                  obrigatorio={true}
                  visivel={episodio.assistido || false}
                  deveExibirErro={deveExibirErro}
                  atualizarValor={this.registrarNota}
                  NomeSerie={episodio.nome}
                />
              </div>
            </div>
          </header>
        </div>
      )
    )
  };
}
