import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import Home from './home';
import Avaliacoes from './avaliacoes';
import TodosEpisodios from './todosEpisodios';
import DetalhesEpisodios from './detalhesEpisodio';

export default class App extends Component {
  render() {
    return (
      <Router>
        <Route path="/" exact component={ Home } />
        <Route path="/avaliacoes" exact component={ Avaliacoes } />
        <Route path="/episodios" exact component={ TodosEpisodios } />
        <Route path="/episodio/:id" exact component={ DetalhesEpisodios } />
      </Router>
    )
  };
}
