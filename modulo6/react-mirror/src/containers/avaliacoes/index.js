import React, { Fragment, Component } from 'react';

import EpisodiosApi from '../../api/episodiosApi';
import { BotaoUi, ListaEpisodiosUi } from '../../components'
import { ListaEpisodios } from '../../models';

export default class ListaAvaliacoes extends Component {
  constructor( props ) {
    super( props );
    this.episodiosApi = new EpisodiosApi();
    this.state = {
      avaliados: []
    }
  }

  componentDidMount() {
    const requisicoes = [
      this.episodiosApi.buscar(),
      this.episodiosApi.buscarTodosDetalhes(),
      this.episodiosApi.buscarTodasNotas()
    ];

    Promise.all( requisicoes )
      .then( respostas => {
        let listaEpisodios = new ListaEpisodios( respostas[0], respostas[2], respostas[1] );
        this.setState( state => {
          return {
            ...state,
            avaliados: listaEpisodios.avaliados
          }
        })
      })
  }

  render() {
    const { avaliados } = this.state;

    return(
      <Fragment>
        <header className="App-header">
          <BotaoUi classe="botao" link="/" nome="Página Inicial" />
          <ListaEpisodiosUi listaEpisodios={ avaliados }/>
        </header>
      </Fragment>
    )
  }

}