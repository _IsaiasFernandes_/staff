  const Mensagens = {
      SUCESSO: {
        REGISTRO_NOTA: 'Registramos sua nota!'
      },
      ERRO: {
        NOTA_INVALIDA: 'Informar uma nota válida (entre 1 e 5)',
        CAMPO_OBRIGATORIO: '* Obrigatório'
      }
    };
    
  export default Mensagens;