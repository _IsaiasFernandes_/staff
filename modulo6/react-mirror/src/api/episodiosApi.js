import { _get, _post, url } from './api';


export default class EpisodiosApi {
  
  async buscar() {
    return _get( `${ url }episodios` );
  }

  async buscarTodosDetalhes( ) {
    return await _get( `${ url }detalhes` );
  }

  async buscarEpisodio( id ) {
    const response = await _get( `${ url }episodios?id=${ id }` );
    return response[ 0 ];
  }

  async buscarDetalhes( id ) {
    const response = await _get( `${ url }episodios/${ id }/detalhes` );
    return response[ 0 ];
  }

  buscarNota( id ) {
    return _get( `${ url }notas?episodioId=${ id }` );
  }

  buscarTodasNotas() {
    return _get( `${ url }notas` );
  }

  async registrarNota( { nota, episodioId } ) {
    const response = await _post( `${ url }notas`, { nota, episodioId } );
    return response[ 0 ];
  }

  filtrarPorTermo( termo ) {
    return _get( `${ url }detalhes?q=${ termo }` );
  }

}