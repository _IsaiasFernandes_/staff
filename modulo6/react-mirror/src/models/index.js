import ListaEpisodios from './listaEpisodios';
import Episodio from './episodio';

export { ListaEpisodios, Episodio };