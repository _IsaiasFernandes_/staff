import React, { Component, Fragment } from 'react';

import './style.css';

export default class EpisodioUi extends Component {
  render() {
    const { episodio } = this.props;
    return (
      <Fragment>
          <h2>{ episodio.nome }</h2>
            <img src={ episodio.url } alt={ episodio.nome }/><br/>
            <div className="informacoes">
            <p><strong>Duração: </strong>{ episodio.duracaoEmMin }</p>
            <p><strong>Temporada-Episódio: </strong>{ episodio.temporadaEpisodio }</p>
            </div>
      </Fragment>
    )
  }
}