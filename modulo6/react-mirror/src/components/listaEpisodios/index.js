import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';

const ListaEpisodios = ( { listaEpisodios } ) =>
  <Fragment>
    <div className="table">
    <div className="cabecalho">
            <div className="nomes">Nome</div>
            <div className="notas">Nota</div>
            <div className="estreia">Estreia</div>
          </div>
    {
      listaEpisodios && listaEpisodios.map( e => 
          <div className="corpo">
          <div key={ e.id }>
          <Link to={{ pathname: `/episodio/${ e.id }`, state: { episodio: e } }}>
              <div className="nome">{ e.nome }</div>
          </Link>
              <div className="nota">{ e.nota }</div>
              <div className="estreia">{ e.dataEstreia.toLocaleDateString( 'pt-BR' ) || 'N/D' }</div>
            </div>
        </div>
      )
    }
    </div>
  </Fragment>

export default ListaEpisodios;