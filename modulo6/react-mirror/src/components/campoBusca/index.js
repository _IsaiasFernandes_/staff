import React, { Fragment } from 'react';

const CampoBusca = ( { placeholder, atualizaValor } ) =>
  <Fragment>
    <input type="text" placeholder={ placeholder } onBlur={ atualizaValor } />
  </Fragment>

export default CampoBusca;