import React, { Component } from "react";
import PropTypes from 'prop-types';

export default class Mensagem extends Component {
  constructor( props ) {
    super( props );
    this.idsTimeouts = [];
    this.animacao = '';
  }
  
  fechar() {
    this.props.atualizarMensagem( false );
  }

  limparTimeouts() {
    this.idsTimeouts.forEach( clearTimeout );
  }

  componentWillUnmount() {
    this.limparTimeouts();
  }

  componentDidUpdate( prevProps ) {
    const { exibir, segundos } = this.props;

    if ( prevProps.exibir !== exibir ) {
      const novoIdTimeout = setTimeout(() => {
        this.fechar();
      }, segundos * 1000 );

      this.idsTimeouts.push( novoIdTimeout );
    }
  }

  render(){
    const { exibir, mensagem, cor } = this.props;

    if( this.animacao || exibir ) {
      this.animacao = exibir ? 'fade-in' : 'fade-out';
    }
    
    return <div onClick={ () => this.fechar() } className={ `flash ${ cor } ${ this.animacao }` }><span>{ mensagem }</span></div>
  }
}

Mensagem.propTypes = {
  mensagem: PropTypes.string.isRequired,
  exibir: PropTypes.bool.isRequired,
  atualizarMensagem: PropTypes.func.isRequired,
  cor: PropTypes.oneOf( [ 'verde', 'vermelho' ] )
}

Mensagem.defaultProps = {
  cor: 'verde',
  segundos: 3
}