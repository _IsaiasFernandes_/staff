import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { Button } from 'antd';

const BotaoUi = ( { classe, metodo, nome, link, state} ) => 
      <Fragment>
      <Button className={ classe } onClick={ metodo }>
      { link ? (<Link to={ link }>{ nome }</Link>) : nome }</Button>
      </Fragment>

BotaoUi.propTypes = {
  nome: PropTypes.string.isRequired,
  metodo: PropTypes.func,
  classe: PropTypes.string
}

export default BotaoUi;