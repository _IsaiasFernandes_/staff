class Jedi {
  //_classePersonagem = "Jedi";

  constructor(nome) {
    this._nome = nome;
    this._estaMorto = false;
  }

  matarJedi() {
    this._estaMorto = true;
  }

  atacarComSabre() {
    setTimeout(() => {
      console.log("Ataquei");
    }, 2000);
  }

  get retornarNome() {
    return this._nome;
  }

  get estaMorto() {
    return this._estaMorto;
  }
}

let sky = new Jedi("Luke");
sky.atacarComSabre();
sky.matarJedi();

console.log(sky.retornarNome);

if (sky.estaMorto) {
  console.log("Ele morreu");
}

