
function calcularCirculo(raio, tipoCalculo) {

  if (tipoCalculo.toUpperCase() === "A") {
    return (raio * raio) * Math.PI;
  }

  if (tipoCalculo.toUpperCase() === "C") {
    return (2 * Math.PI) * raio;
  }

  return null;
}

console.log(calcularCirculo(5, "C"));

