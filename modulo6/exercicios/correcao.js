/* Ex 1 */

let circulo = {
  raio: 3,
  tipoCalculo: "A"
}

function calcularCirculo({ raio, tipoCalculo: tipo }) {
  return Math.ceil(tipo == "A" ? Math.PI * Math.pow(raio, 2) : 2 * Math.PI * raio);
}

console.log(calcularCirculo(circulo));

/* Ex 2 */

function naoBissexto(ano) {
  return (ano % 400 == 0) || (ano % 4 == 0 && ano % 100 != 0) ? false : true;
}

console.log(naoBissexto(2016));
console.log(naoBissexto(2017));

/* Ex 3 */

function somarPares(array) {
  let soma = 0;

  for (let i = 0; i < array.length; i += 2) {
    soma += array[i];
  }
  return soma;
}

console.log(somarPares([1, 56, 4.34, 6, -2]));

/* Ex 4 */

/* function adicionar(valorA) {
  return (valorB) => { return valorB + valorA; }
} */
/* let adicionar = op1 => op2 => op1 + op2;
let is_divisivel = (divisor, numero) => !(numero % divisor);
const divisor = 2;

console.log(adicionar(4)(3));
console.log(is_divisivel(divisor, 5));
console.log(is_divisivel(divisor, 8));
console.log(is_divisivel(divisor, 16));
console.log(is_divisivel(divisor, 9)); */

let divisivelPor = divisor => numero => !(numero % divisor);
let is_divisivel = divisivelPor(2);

console.log(is_divisivel(5));
console.log(is_divisivel(8));
console.log(is_divisivel(16));
console.log(is_divisivel(9));

/* Ex 5 */

let moedas = (function () {
  //tudo escrito aqui é privado;
  function imprimirMoeda(params) {
    function arredondar(numero, precisao = 2) {
      const fator = Math.pow(10, precisao);
      return Math.ceil(numero * fator) / fator;
    }

    const {
      numero,
      separadorMilhar,
      separadorDecimal,
      colocarMoeda,
      colocarNegativo
    } = params;

    const qtdCasasMilhares = 3;
    let StringBuffer = [];
    let parteDecimal = arredondar(Math.abs(numero) % 1);
    let parteInteira = Math.trunc(numero);
    let parteInteiraString = Math.abs(parteInteira).toString();
    let tamanhoParteInteira = parteInteiraString.length;

    //.477 .313 2
    let contador = 1;
    while (parteInteiraString.length > 0) {
      if (contador % qtdCasasMilhares == 0 && parteInteiraString.length > 3) {
        StringBuffer.push(`${separadorMilhar}${parteInteiraString.slice(tamanhoParteInteira - contador)}`);
        parteInteiraString = parteInteiraString.slice(0, tamanhoParteInteira - contador);
      } else if (parteInteiraString.length <= qtdCasasMilhares) {
        StringBuffer.push(parteInteiraString);
        parteInteiraString = '';
      }
      contador++;
    }
    StringBuffer.push(parteInteiraString);

    let decimalString = parteDecimal.toString().replace('0.', '').padStart(2, 0);
    const numeroFormatado = `${StringBuffer.reverse().join('')}${separadorDecimal}${decimalString}`;

    return parteInteira >= 0 ? colocarMoeda(numeroFormatado) : colocarNegativo(numeroFormatado);

  }

  //Tudo que eu escrever no return é publico;
  return {
    imprimirBRL: (numero) =>
      imprimirMoeda({
        numero,
        separadorMilhar: '.',
        separadorDecimal: ',',
        colocarMoeda: numeroFormatado => `R$ ${numeroFormatado}`,
        colocarNegativo: numeroFormatado => `-R$ ${numeroFormatado}`
      }),
    imprimirGBP: (numero) =>
      imprimirMoeda({
        numero,
        separadorMilhar: ',',
        separadorDecimal: '.',
        colocarMoeda: numeroFormatado => `£ ${numeroFormatado}`,
        colocarNegativo: numeroFormatado => `-£ ${numeroFormatado}`
      }),
    imprimirFR: (numero) =>
      imprimirMoeda({
        numero,
        separadorMilhar: '.',
        separadorDecimal: ',',
        colocarMoeda: numeroFormatado => `${numeroFormatado} €`,
        colocarNegativo: numeroFormatado => `-${numeroFormatado} €`
      })
  }

})()

console.log(moedas.imprimirBRL(2313477.0135));
console.log(moedas.imprimirBRL(477.0135));
console.log(moedas.imprimirGBP(2313477.0135));
console.log(moedas.imprimirGBP(477.0135));
console.log(moedas.imprimirFR(2313477.0135));
console.log(moedas.imprimirFR(477.0135));

