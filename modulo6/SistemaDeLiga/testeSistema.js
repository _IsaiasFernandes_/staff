let INTZ = new Times("INTZ", "LOL", "Classificado", "Liga Mundial");

let jogadorINTZ1 = new Jogador("Tay", 1);
let jogadorINTZ2 = new Jogador("Shini", 2);
let jogadorINTZ3 = new Jogador("Envy", 3);

INTZ.adicionarJogador(jogadorINTZ1);
INTZ.adicionarJogador(jogadorINTZ2);
INTZ.adicionarJogador(jogadorINTZ3);
console.log(INTZ.buscarJogadorNumero(3));
console.log(INTZ.buscarJogadorNome(jogadorINTZ2.nome));
console.log(INTZ.buscarJogadorNome(jogadorINTZ3.nome));

let LGD = new Times("LGD", "LOL", "Classificado", "Liga Mundial");

let jogadorLGD1 = new Jogador("Langx", 1);
let jogadorLGD2 = new Jogador("Peanut", 2);
let jogadorLGD3 = new Jogador("Xiye", 3);

LGD.adicionarJogador(jogadorLGD1);
LGD.adicionarJogador(jogadorLGD2);
LGD.adicionarJogador(jogadorLGD3);
console.log(LGD.buscarJogadorNumero(2));
console.log(LGD.buscarJogadorNome(jogadorLGD2.nome));

let historico = [];

let partidas = new Partidas(INTZ, LGD);

for (let i = 0; i < 10; i++) {
  historico.push(partidas.jogarPartida());
}

console.log(historico);