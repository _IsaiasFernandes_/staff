class Partidas {

  constructor(timeA, timeB) {
    this._timeA = timeA;
    this._timeB = timeB;
  }

  get timeA() {
    return this._timeA;
  }

  get timeB() {
    return this._timeB;
  }

  resultadoPartida(nomeTimeA, nomeTimeB) {
    let resultadoA = (Math.random() * (10 - 1) + 1);
    let resultadoB = (Math.random() * (10 - 1) + 1);
    if (resultadoB === resultadoA) {
      while (resultadoB === resultadoA) {
        resultadoB = (Math.random() * (10 - 1) + 1);
      }
    }
    if (resultadoA > resultadoB) {
      return `${nomeTimeA} venceu do ${nomeTimeB}`;
    } else {
      return `${nomeTimeB} venceu do ${nomeTimeA}`;
    }
  }

  jogarPartida() {
    return this.resultadoPartida(this._timeA.nome, this._timeB.nome);
  }
}