class Times {

  constructor(nome, tipoEsporte, status, ligaJoga) {
    this._nome = nome;
    this._tipoEsporte = tipoEsporte;
    this._status = status;
    this._ligaJoga = ligaJoga;
    this._jogador = [];
  }

  adicionarJogador(jogador) {
    this._jogador.push(jogador);
  }

  buscarJogadorNome(nome) {
    return this._jogador.filter(jogador => jogador.nome === nome);
  }

  buscarJogadorNumero(numero) {
    return this._jogador.filter(jogador => jogador.numero === numero);
  }

  get nome() {
    return this._nome;
  }

  get tipoEsporte() {
    return this._tipoEsporte;
  }

  get status() {
    return this._status;
  }

  get ligaJoga() {
    return this._ligaJoga;
  }
}