import React from 'react';
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';
import enjoei from '../../assets/unnamed.png';

import './style.css';

export default function Headerzinho() {
  return(
    <div className="header">
      <span>
        <KeyboardBackspaceIcon />
      </span>
      <div className="imagem">
        <img src={ enjoei } className="imagem-menu" alt="enjoei"/>
      </div>
    </div>
  )
}