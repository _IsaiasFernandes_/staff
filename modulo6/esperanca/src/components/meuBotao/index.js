import React from 'react';

import './style.css';

export default function Botao(props) {
  return (
  <button className={props.classe} onClick={props.metodo}>{props.nome}</button>
  )
}