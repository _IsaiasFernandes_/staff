import Avaliacao from './avaliacao';
import Botao from './meuBotao';
import Carousel from './carousel';
import Comentarios from './comentarios';
import Headerzinho from './headerzinho';
import Menu from './menu';
import Mensagem from './mensagem';
import Lista from './lista';
import { PrivateRoute } from './privateRoute';
import UnicoProduto from './unicoProduto';
import InfoProduto from './infoProdutos';

export { Avaliacao, Botao, Carousel, Comentarios, Headerzinho, InfoProduto, Menu, Mensagem, Lista, PrivateRoute, UnicoProduto };