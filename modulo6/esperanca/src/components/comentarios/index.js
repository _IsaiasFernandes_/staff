import React, { useEffect, useState } from 'react';
import { Botao } from '../';
import Loja from '../../service/loja';
import NegoNey from '../../assets/1_negoney-10285790.jpg';

import './style.css';

export default function Comentarios(props) {
  const nomeUser = 'teste';
  const { idProduto, nomeProduto } = props;
  const loja = new Loja();
  const [comentario, setComentario] = useState('');
  const [comentarios, setComentarios] = useState([])

  useEffect(() => {
    loja.buscarComentariosProduto(idProduto).then(res => setComentarios( res ));
  }, []);

  function handleRegister() {
    loja.registrarComentario({
      nomeUser,
      comentario,
      idProduto,
      nomeProduto,
    })
    setComentario('');
    loja.buscarComentariosProduto(idProduto).then(res => setComentarios(res))
  }

  function reverse( comentarios ) {
    let comentarioReverso = []
    comentarios.map( com => {
      comentarioReverso.push(`${com.nomeUser}: ${com.comentario}`);
    })
    return (
      comentarioReverso.reverse().map( com => <p>{ com }</p>)
    )
  }

  return (
    <div className="perguntas">
      <div className="input">
        <input type="text" placeholder="pergunte ao vendedor" value={ comentario } onChange={event => setComentario(event.target.value)} maxLength="280" />
        <Botao type='submit' nome={'perguntar'} 
        metodo={ handleRegister } 
        classe={'branco'} />
      </div>
      <div className="comentarios">
      <h3>últimas perguntas</h3>
      <div className="comentarios-table">
      { comentarios && ( reverse(comentarios) ) }
      </div>
      </div>
    </div>
  )
}