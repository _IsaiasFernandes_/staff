import React from 'react';
import enjoei from '../../assets/unnamed.png';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import ContactSupportOutlinedIcon from '@material-ui/icons/ContactSupportOutlined';
import { Botao } from '../';

import "./style.css";

export default function Menu() {
  return (
    <div className="menu">
      <div className="procura">
        <img src={enjoei} className="imagem-menu" alt="enjoei" />
        <div className="pesquisar">
          <InputBase
            className="busca"
            placeholder='Busque "ello"'
            inputProps={{ 'aria-label': 'search google maps' }}
          />
          <SearchIcon />
        </div>
      </div>
      <div className="navegacao">
        <ul>
          <li><span>moças</span></li>
          <li><span>rapazes</span></li>
          <li><span>kids</span></li>
          <li><span>casa&tal</span></li>
        </ul>
      </div>
      <div className="botoes">
        <ContactSupportOutlinedIcon />
        <span>entrar</span>
        <Botao classe={'rosa'} nome={'quero vender'} />
      </div>
    </div>
  )
}