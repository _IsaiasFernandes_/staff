import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';

import './style.css';

export default function Lista(props) {
  const { categoria, produtos } = props;
  return (
    <Fragment>
      <section className="lista">
        <div className="titulo">
          <h2>{categoria.nome}</h2>
          <div className="subTitulo"><span>{categoria.subTitulo}</span>{'ver mais >'}</div>
        </div>
        <div className="band">
          {
            produtos.map(prod => {
              return (
                <Link className="item" key={prod.id} to={`/produto/${prod.id}`} >
                  <img src={prod.imagem} />
                  <span>R${prod.preco}</span>
                </Link>
              )
            })}
        </div>
      </section>
    </Fragment>
  )
}