import React, { Fragment, useEffect, useState } from 'react';
import SentimentSatisfiedAltIcon from '@material-ui/icons/SentimentSatisfiedAlt';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import { Botao, Comentarios, Avaliacao, InfoProduto } from '../';
import Loja from '../../service/loja';


import './style.css'

export default function UnicoProduto(props) {
  const { produto, detalhes } = props;
  const [vendedor, setVendedor] = useState([]);
  const [descricao, setDescricao] = useState([]);

  useEffect(() => {
    const loja = new Loja();
    loja.buscarVendedor(produto.idVendedor).then(res => setVendedor(res));
  }, []);

  useEffect(() => {
    const { descricao } = detalhes;
    setDescricao( descricao.split('\n') );
  }, [detalhes])

  function valorParcelador(preco) {
    return preco / 10;
  }

  return (
    <Fragment>
      { vendedor && (
        <div className="container">
          <div className="container-left">
            <img src={produto.imagem} alt={produto.nome} />
            <Comentarios idProduto={produto.id} nomeProduto={produto.nome}/>
          </div>
          <div className="container-middle">
            <ThumbUpIcon className="like" />
            <SentimentSatisfiedAltIcon className="happy" />
          </div>
          <div className="container-right">
            <div className="localizacao">moças / acessórios / cintos</div>
            <div className="nome-produto"><h2>{produto.nome}</h2></div>
            <div className="nome-vendedor"> {vendedor.nome} <span>seguir marca</span></div>
            <div className="valor">
              <span className="valor-desconto">R${produto.preco}</span> <s>R$ {produto.preco + 10}</s>
              <span className="desconto">R$ 10 na 1° compra</span>
            </div>
            <span className="parcela">10x R${valorParcelador(produto.preco)} sem juros</span>
            <p>cartões</p>
            <div className="botoes">
              <Botao nome={'eu quero'} classe={'rosa'} />
              <Botao nome={'adicionar à sacolinha'} classe={'branco'} />
              <Botao nome={'fazer oferta'} classe={'branco'} />
            </div>
            <InfoProduto detalhes={detalhes} />
            <div className="descricao">
              <span className="titulo">descrição</span>
              <span>
                { descricao &&
                  (descricao.map(elem => {
                    return (
                    <p>
                      { elem }
                    </p>)
                  }
                  ))
                }
              </span>
            </div>
            <Avaliacao vendedor={vendedor} />
          </div>
        </div>
      )
      }

    </Fragment>
  )
}