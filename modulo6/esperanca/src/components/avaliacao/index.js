import React, { Fragment, useState, useEffect } from 'react';

import { Botao } from '../';
import Rating from '@material-ui/lab/Rating';

import './style.css';

export default function Avaliacao(props) {
  const { vendedor } = props;

  function dataFormatada( criacao ) {
    let data = criacao.split('-');
    let meses = ["jan", "fev", "mar", "abr", "mai", "jun", "jul", "ago",
      "set", "out", "nov", "dez"];
    let dataFormatada = `${meses[data[2] - 1]}/${data[0]}`;
    return dataFormatada;
  }

  return (
    <Fragment>
      { vendedor && (
        <div className="avaliacao">
          <div className="table-up">
            <span>{vendedor.nome}</span>
            <Botao nome={'seguir'} classe='branco' />
          </div>
          <div className="table-middle">
            <div className="nota">
              <span>avaliação</span>
              <Rating name="size-small" defaultValue={2} size="small" value={5} readOnly />
            </div>
            <div className="entregas">
              <span>ultimas entregas</span>
              <Rating name="size-small" defaultValue={2} size="small" value={5} readOnly />
            </div>
            <div className="tempo">
              <span>tempo médio de envio</span>
              <p>{vendedor.tempoMedio === '' ? 'N/D' : vendedor.tempoMedio}</p>
            </div>
          </div>
          <div className="table-bottom">
            <div className="venda">
              <span>à venda</span>
              <p> 41</p>
            </div>
            <div className="vendidos">
              <span>vendidos</span>
              <p>{ vendedor.qtdItensVendidos }</p>
            </div>
            <div className="tempo-vendedor">
              <span>no enjoei desde</span>
              <p>{ vendedor.criacao && (dataFormatada( vendedor.criacao )) }</p>
            </div>
          </div>
        </div>
      )}</Fragment>
  )
}