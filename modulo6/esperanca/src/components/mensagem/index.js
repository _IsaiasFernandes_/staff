import React from 'react';
import LocalShippingOutlinedIcon from '@material-ui/icons/LocalShippingOutlined';

import './style.css';
import { grey } from '@material-ui/core/colors';

export default function Mensagem() {
  return (
    <div className="mensagem">
    <LocalShippingOutlinedIcon style={{ color: grey[50] }}/>
    <div className="mensagens">
    <span>frete grátis na primeira compra - </span>
    <span>promo válida no frete padrão até R$20</span>
    </div>
    </div>
  )
}