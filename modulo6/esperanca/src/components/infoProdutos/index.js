import React, { Fragment, useEffect, useState } from 'react';

export default function InformacoesProduto(props) {
  const { detalhes } = props;
  const [tamanho, setTamanho] = useState('');

  useEffect(() => {
    let valoresDetalhes = detalhes.descricao.split('');
    setTamanho(valoresDetalhes[2])
  })

  function formatarDetalhes() {
  }
  return (
    <Fragment >
      { detalhes && (
        <div className="informacoes-produtos">
          <div className="tamanho">
            <span >tamanho</span>
            <p>P</p>
          </div>
          <div className="informacoes-produtos-abaixo">
            <div className="marca">
              <span>marca</span>
              <p>{detalhes.marca === '' ? 'N/D' : detalhes.marca}</p>
            </div>
            <div className="condicao">
              <span>condição</span>
              <p>{detalhes.condicao}</p>
            </div>
            <div className="marca">
              <span>código</span>
              <p>{detalhes.codigo}</p>
            </div>
          </div>
        </div>
      )}
    </Fragment>
  )
}