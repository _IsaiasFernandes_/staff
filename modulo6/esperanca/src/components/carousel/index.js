import React, { useEffect, useState } from 'react';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
import './style.css';
import Loja from '../../service/loja';

export default function DemoCarousel() {
  const loja = new Loja();
  const [banner, setBanner] = useState([]);

  useEffect(() => {
    loja.buscarBanners().then( res => setBanner(res));
  }, [])

  return (
    <Carousel  className="carousel">
      { banner && (banner.map(ban => {
        return (
          <div>
            <img src={ban.imagem} />
          </div>
        )
      }))}
    </Carousel>
  );
}