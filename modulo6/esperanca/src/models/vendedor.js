
export default class Vendedor {
  constructor(id, nome, cidade, estado, tempoMedio, qtdItensVendidos, criacao) {
    this._id = id;
    this._nome = nome;
    this._cidade = cidade;
    this._estado = estado;
    this._tempoMedio = tempoMedio;
    this._qtdItensVendidos = qtdItensVendidos;
    this._criacao = criacao;
  }
}