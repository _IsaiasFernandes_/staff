import { Categoria, Produto } from '.';

export default class CategoriasEProdutos {
  constructor( categorias = [], produtos = [] ) {
    this._todasCategorias = categorias.map( elem => new Categoria( elem.id, elem.nome, elem.subTitulo ) );
    this._todosProdutos = produtos.map( elem => new Produto( elem.id, elem.idVendedor, elem.idCategoria, elem.nome, elem.preco, elem.imagem ) );
  }
}