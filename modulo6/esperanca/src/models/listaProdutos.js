import Produto from './produto';

export default class ListaProdutos {
  constructor( listaProdutos = [] ) {
    this._todos = listaProdutos.map( elem => new Produto(  elem.id, elem.idVendedor, elem.idCategoria, elem.nome, elem.preco, elem.imagem ));
  }
}