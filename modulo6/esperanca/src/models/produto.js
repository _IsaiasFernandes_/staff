
export default class Produto{
  constructor( id, idVendedor, idCategoria, nome, preco, imagem ) {
    this.id = id;
    this.idVendedor = idVendedor;
    this.idCategoria = idCategoria;
    this.nome = nome;
    this.preco = preco;
    this.imagem = imagem;
  }
}
