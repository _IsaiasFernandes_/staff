
export default class Categoria {
  constructor(id, nome, subTitulo) {
    this._id = id;
    this._nome = nome;
    this._subTitulo = subTitulo;
  }
}