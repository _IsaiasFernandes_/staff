import React, { Fragment } from 'react';
import Routes from './routes';
import { Footer } from './pages';

export default function App() {
  return (
    <Fragment>
      <Routes />
      <Footer />
    </Fragment>
  );
}
