import React, { Component, Fragment } from 'react';
import { Carousel, Mensagem, Menu, Lista } from '../../components';
import { CategoriasEProdutos } from '../../models';
import Loja from '../../service/loja';


class Home extends Component {

  constructor(props) {
    super(props);
    this.loja = new Loja();
    this.produtosPorCategoria = new CategoriasEProdutos; 
    this.state = {
      produtos: [],
      categorias: [],
    }
  }

  componentDidMount() {
    let produtos = []
    let categorias = []

    const requisicoes = [
      this.loja.buscarProdutos(),
      this.loja.buscarCategorias()
    ];

    Promise.all(requisicoes).then((resp) => {
      produtos = resp[0];
      categorias = resp[1];
      this.setState((state) => {
        return {
          ...state,
          produtos,
          categorias
        };
      });
    });
  }

  render() {
    const { produtos, categorias } = this.state;
    return (
      <Fragment>
        <Mensagem />
        <Menu />
        <Carousel />
        { 
          categorias.map( cat => {
            let produtosFiltrados = []
            produtos.forEach( prod => {
              if( prod.idCategoria === cat.id ) {
                produtosFiltrados.push(prod);
              }
            })
            return ( cat && (
              <Lista key={cat.id} categoria={cat} produtos={produtosFiltrados} />
            ) 
            )
          })
        }
      </Fragment>
    );
  }
}

export default Home;