import React, { Fragment, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Botao, Headerzinho } from '../../components';
import Usuarios from '../../service/usuarios';

import './style.css';


export default function Formulario() {
  const history = useHistory();
  const cadastro = new Usuarios();
  const [ nome, setNome ] = useState('');
  const [ senha, setSenha ] = useState('');

  function handleRegister(event) {
    event.preventDefault();
    try {
    cadastro.registrarUsuario(nome, senha);
    setNome('');
    setSenha('');
    history.push('/login');
    } catch (err) {
      alert("Não ofi possivel cadastrar");
    }
  }

  return (
    <Fragment>
      <Headerzinho />
      <section className="secao">
        <div className="login">
          <h1>Cadastre-se no enjoei</h1>
          <div className="cadastro">
            <Botao classe={'azul'} nome={'entre usando o facebook'} />
            <span>ou</span>
            <form onSubmit={ env => handleRegister(env) } className="formulario">
              <label htmlFor="">Nome:</label>
              <input 
                type="text"
                value={ nome }
                onChange={ event => setNome(event.target.value) }
              />
              <label htmlFor="">Senha super secreta:</label>
              <input 
                type="password"
                value={ senha }
                onChange={ event => setSenha(event.target.value) } 
              />
              <div className="check-senha">
                <div className="checkbox">
                  <input type="checkbox" />
                  <label htmlFor=""> continuar conectado</label>
                </div>
                <span >esqueci a senha</span>
              </div>
              <Botao type="submit"classe={'vermelho'} nome={'Cadastrar'} />
            </form>
          </div>
        </div>
      </section>
    </Fragment>
  )
}


