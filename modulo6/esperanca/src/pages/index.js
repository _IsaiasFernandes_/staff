import Home from './Home';
import Interna from './Interna';
import Login from './Login';
import Formulario from './Formulario';
import Footer from './Footer';

export { Home, Interna, Login, Formulario, Footer };