import React, { Fragment, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Botao, Headerzinho } from '../../components';
import Usuarios from '../../service/usuarios';
import './style.css';


export default function Login(props) {
  const cadastro = new Usuarios();
  const [nome, setNome] = useState('');
  const [senha, setSenha] = useState('');
  const [validar, setValidar] = useState('');


  function handleLogin(event) {
    event.preventDefault();
    if( nome === '' || senha === '') {
      alert('Campo nome ou senha esta vazio');
      props.history.push('/login');
    }
    cadastro.buscarUsuario(nome, senha).then(res => setValidar(res));
    
    if ( validar.nome !== nome) {
      alert("Não foi possivel encontrar sua conta");
    } else {
      localStorage.setItem('user', 'logado');
      props.history.push('/');
    }
  }

  return (
    <Fragment>
      <Headerzinho />      
         <section className="secao">
         <div className="login">
           <h1>faça login no enjoei</h1>
           <div className="cadastro">
             <Botao classe={'azul'} nome={'entre usando o facebook'} />
             <span>ou</span>
             <form action="" className="formulario" onSubmit={event => handleLogin(event)} >
               <label htmlFor="">Nome:</label>
               <input
                 type="text"
                 value={nome}
                 onChange={event => setNome(event.target.value)}
               />
               <label htmlFor="">Senha super secreta:</label>
               <input
                 type="password"
                 value={senha}
                 onChange={event => setSenha(event.target.value)}
               />
               <div className="check-senha">
                 <div className="checkbox">
                   <input type="checkbox" />
                   <label htmlFor=""> continuar conectado</label>
                 </div>
                 <span >esqueci a senha</span>
               </div>
               <Botao classe={'vermelho'} nome={'entrar'} />
             </form>
             <Link className="cadastrar" to="/cadastrar">não tenho cadastro</Link>
           </div>
         </div>
       </section>
    </Fragment>
  )
}
