import React, { Component } from 'react';
import { Menu, Mensagem, UnicoProduto, Lista } from '../../components';
import Loja from '../../service/loja';

import './style.css';

class Interna extends Component {
  constructor(props) {
    super(props);
    this.loja = new Loja();
    this.state = {
      produto: null,
      detalhes: null,
      categorias: [],
      produtoList: []
    }
  }

  componentWillMount() {
    const idProduto = this.props.match.params.id; 
    const requisicoes = [
      this.loja.buscarProduto(idProduto),
      this.loja.buscarDetalhe(idProduto),
      this.loja.buscarCategorias(idProduto),
      this.loja.buscarProdutos()
    ]
    
    Promise.all(requisicoes)
    .then(resp => {
      this.setState( (state) => {
        return {
          ...state,
          produto: resp[0],
          detalhes: resp[1],
          categorias: resp[2],
          produtoList: resp[3]
        }
      })
    })
  }

  render() {
    const { produto, detalhes, categorias, produtoList } = this.state;
    return (
      <div className="interna">
        <Mensagem />
        <Menu />
        { produto && (
          <UnicoProduto produto={ produto } detalhes={ detalhes } />
        )} 
        { produto && (
          categorias.map( cat => {
            let produtosFiltrados = []
            produtoList.forEach( prod => {
              if( prod.idCategoria === cat.id ) {
                produtosFiltrados.push(prod);
              }
            })
            return ( cat && (
              <Lista key={cat.id} categoria={cat} produtos={produtosFiltrados} />
            ) 
            )
          })
        )}
         
      </div>
    );
  }
}

export default Interna;