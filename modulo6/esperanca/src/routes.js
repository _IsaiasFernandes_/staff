import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Home, Interna, Login, Formulario } from './pages';
import { PrivateRoute } from './components';

export default function Routes() {
  return (
    <Router>
      <Route exact path='/' component={ Home } />
      <Route exact path='/login' component={ Login } />
      <Route exact path='/cadastrar' component={ Formulario } />
      <PrivateRoute exact path='/produto/:id' component={ Interna } />
    </Router>
  )  
}