import { url, _get, _post } from './api';

export default class Loja {
  
  async buscarProdutos() {
    return await _get( `${ url }produtos` );
  }

  async buscarProduto( id ) {
    const response = await _get( `${ url }produtos?id=${ id }` );
    return response[0];
  }
  
  async buscarDetalhes() {
    return await _get( `${ url }detalhes` );
  }


  async buscarDetalhe( id ) {
    const response = await _get( `${ url }detalhes?idProduto=${ id }` );
    return response[0];
  }

  async buscarCategorias() {
    return await _get( `${ url }categorias` );
  }

  async buscarCategoria( id ) {
    const response =  _get( `${ url }produtos?id=${ id }`);
    return response[0];
  }
  
  async buscarComentariosProduto( id ) {
    return await _get( `${ url }comentarios?idProduto=${ id }` );
  }

  async registrarComentario({ nomeUser, comentario, idProduto, nomeProduto }) {
    const response = await _post( `${ url }comentarios`,{ nomeUser, comentario, idProduto, nomeProduto } );
    return response[0];
  }

  async buscarVendedor( id ) {
    const response = await _get( `${ url }vendedor/?id=${ id }` );
    return response[0];
  }

  async buscarBanners() {
    return await _get( `${ url }banners` );
  }
}