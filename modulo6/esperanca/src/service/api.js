import axios from 'axios';

const url = 'http://localhost:9000/';
const _get = url => new Promise( (resolve, reject ) => axios.get( url ).then( response => resolve(response.data) ) );
const _post = ( url, dados ) => new Promise( (resolve, reject ) => axios.post( url, dados ).then( response => resolve(response.data) ) );

export { url, _get, _post };


