import { url, _get, _post} from './api';

export default class Usuarios {

  async buscarUsuario( nome, senha ) {
    let response = await _get( `${ url }usuarios?nome=${nome}&senha=${senha}`)
    return response[0];
  }

  async registrarUsuario( nome, senha ) {
    const response = await _post( `${ url }usuarios`, { nome, senha } );
    return response[0];
  }

}