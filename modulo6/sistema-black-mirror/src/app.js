import React, { Fragment, Component } from 'react'
import { CampoBusca as Menu } from './components';
import Routes from './routes';
import { BrowserRouter as Router } from 'react-router-dom'

import './general.css';

export default class App extends Component {

  render() {
    return (
      <Fragment>
        <Router>
          <Menu />
          <Routes />
        </Router>
      </Fragment>
    )
  }
}
