import React, { Fragment, useEffect, useState } from 'react';
import EpisodiosApi from '../../api/episodiosApi';
import Rating from '@material-ui/lab/Rating';

export default function Estrelas( props ) {
  const { nota, id } = props;
  const [estrela, setEstrela] = useState( nota );
  
/*   useEffect ( () => { 
    this.episodioApi = new EpisodiosApi()
    this.episodioApi.registrarNota( estrela, id );
  }, [estrela])
 */
  return(
    <Fragment>
      <Rating
          name="simple-controlled"
          value={ estrela }
          precision= { props.precisao }
          /* onChange={( event ) => {
            setEstrela(event.target.value)        
          }} */
        />
    </Fragment>
  )
}