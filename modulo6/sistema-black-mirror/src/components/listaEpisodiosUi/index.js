import React, { Component, Fragment } from 'react';
import Rating from '@material-ui/lab/Rating';


import { EpisodioUi, BotaoUi } from '../';

export default class ListaEpisodiosUi extends Component {
  constructor( props ) {
    super(props);
  }

  render() {
    const { listaEpisodios } = this.props;
    return (
      <Fragment>
        {
          listaEpisodios && listaEpisodios.map(e =>
            <div key={e.id}>
              <div className="corpo">
                <EpisodioUi episodio={ e } />
                <Rating name="read-only" value={ e.nota } readOnly />
                <BotaoUi link={ { pathname: `/episodio/${ e.id }`, state: { episodio: e } } } 
                  nome='Ver Detalhes'/>
              </div>
            </div>
          )
        }
      </Fragment>
    )
  }
}