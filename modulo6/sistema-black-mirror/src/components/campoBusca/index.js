import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import BotaoUi from '../botaoUi';
import BM from '../../assets/black-mirror.png';
import { ListaEpisodios } from '../../models';

import './style.css';

export default function CampoBusca ({ placeholder, atualizaValor }) {

  let sortear = () => {
    const listaEpisodios = new ListaEpisodios();
    const episodio = listaEpisodios.episodiosAleatorios;
    return episodio;
  }

  return (
    
    <div className="navegacao">
      <ul>
        <li>
          <Link link="/"><img src={ BM } className="imagem" alt='Black Mirror' /></Link>
        </li>
        <li className="nav">
              <div>
              <BotaoUi nome="Assistir aleatório" metodo={ sortear } />
            </div>
            <div>
              <BotaoUi nome="Ranking" link="/ranking" />
            </div>
        </li>
        
        <li className="pesquisa">
          <span>Bucar Episodio </span>
          <input type="text" placeholder='Buscar' onBlur={atualizaValor} />
        </li>
      </ul>
    </div>
  )
}
