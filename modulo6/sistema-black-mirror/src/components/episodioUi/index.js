import React, { Component, Fragment } from 'react';

export default class EpisodioUi extends Component {
  constructor(props) {
    super(props);
    //this.novoValor = this.novoValor.bind( this );
  }
/*   
  novoValor( newValue ) {
    const { atuatiza } = this.props;
    atuatiza( newValue );
  }
   */
  render() {
    const { episodio } = this.props;
    return (
      <Fragment>
        <img src={ episodio.url } alt={ episodio.nome } className="thumb" />
                <div className="nome">{ episodio.nome }</div>
                <div className="sinopse">{ episodio.sinopse }</div>
                <div className="duracao">{ episodio.duracao }min</div>
      </Fragment>
    )
  }
}
