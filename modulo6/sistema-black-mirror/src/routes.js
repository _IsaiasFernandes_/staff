import React from 'react';
import { Route } from 'react-router-dom';
import { RotaPrivada } from './components';

import { Home, DetalhesEpisodios, Ranking, Login, Register } from './pages';

export default function Routes() {
  return (
      <>
        <Route exact path="/" component={ Home }/>
        <Route exact path="/episodio/:id" component={ DetalhesEpisodios } />
        <Route exact path="/ranking" component={ Ranking } />
        <Route exact path="/login" component={ Login } />
        <Route exact path="/register" component={ Register } />
      </>
  )
}