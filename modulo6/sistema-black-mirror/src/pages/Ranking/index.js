import React, { Component } from 'react';
import { EpisodioUi } from '../../components';
import EpisodiosApi from '../../api/episodiosApi';
import { ListaEpisodios } from '../../models';

export default class Ranking extends Component {
  constructor( props ) {
    super( props );
    this.episodiosApi = new EpisodiosApi();
    this.state = {
      listaEpisodios: []
    }
  }

  componentDidMount() {
    const episodioId = this.props.match.params.id;
    const requisicoes = [
      this.episodiosApi.buscarEpisodio( episodioId ),
      this.episodiosApi.buscarTodasNotas(),
      this.episodiosApi.buscarTodosDetalhes()
    ];

    Promise.all( requisicoes )
      .then( respostas => {
        const episodiosDoServidor = respostas[ 0 ];
        const notasServidor = respostas[ 1 ];
        const detalhesDoServidor = respostas[ 2 ];
        const listaEpisodiosNova = new ListaEpisodios( episodiosDoServidor, notasServidor, detalhesDoServidor );
        this.setState( state => { return { ...state, listaEpisodios: listaEpisodiosNova._todos, notasServidor } } );
      } )
  }

  mediaNotasParaRanking = () => {
    const { listaEpisodios, notasServidor } = this.state;
    const listaNotas = [];

    listaEpisodios.forEach( element => {
      let tot = 0;
      let tam = 0;
      notasServidor.forEach( nota => {
        if( nota.episodioId === element.id ) {
          tot += nota.nota;
          tam++;
        }
      })

      let mediaEpisodio = tot / tam;
      if( !mediaEpisodio ) {
        mediaEpisodio = 0;
      }  
      listaNotas.push( { id: element.id, media: mediaEpisodio } )
    });

    listaNotas.sort( ( a, b ) => {
      return b.media - a.media;
    })

    let eps = [];
    listaNotas.forEach( nota => {
      listaEpisodios.forEach( ep => {
        if( ep.id === nota.id ) {
          ep.media = nota.media;
          eps.push( ep );
        }
      } )
    } )
    return eps; 
  }

  render() {
    const lista = this.mediaNotasParaRanking();
    return (    
      <div>
        <div className='ranking'>
          {lista.map( element => {
            return (
              <div className='ranking' key={ element.id }>
                <EpisodioUi episodio={ element } />
              </div>
            )
          } )}
        </div>
      </div>
    )
  }
}