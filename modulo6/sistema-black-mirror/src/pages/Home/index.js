import React, { Component, Fragment } from 'react';

import EpisodiosApi from '../../api/episodiosApi';
import { ListaEpisodiosUi } from '../../components';
import { ListaEpisodios } from '../../models';



export default class TodosEpisodios extends Component {

  constructor( props ) {
    super( props );
    this.episodiosApi = new EpisodiosApi();
    this.state = {
      ordenacao: () => { },
      tipoOrdenacaoDataEstreia: 'ASC',
      tipoOrdenacaoDuracao: 'ASC',
      listaEpisodios: []
    }
  }

  componentDidMount() {
    const requisicoes = [
      this.episodiosApi.buscar(),
      this.episodiosApi.buscarTodosDetalhes(),
      this.episodiosApi.buscarTodasNotas()
    ];

    Promise.all( requisicoes )
      .then( respostas => {
        let listaEpisodios = new ListaEpisodios( respostas[0], respostas[2], respostas[1] );
        this.setState( state => {
          return {
            ...state,
            listaEpisodios: listaEpisodios._todos.concat([])
          }
        })
      })
  }

  alterarOrdenacaoParaDataEstreia = () => {
    const { tipoOrdenacaoDataEstreia } = this.state;
    this.setState( {
      ordenacao: ( a, b ) => ( tipoOrdenacaoDataEstreia === 'ASC' ? a : b ).dataEstreia  - ( tipoOrdenacaoDataEstreia === 'ASC' ? b : a ).dataEstreia,
      tipoOrdenacaoDataEstreia: tipoOrdenacaoDataEstreia === 'ASC' ? 'DESC' : 'ASC'
    } );
  }

  alterarOrdenacaoParaDuracao = () => {
    const { tipoOrdenacaoDuracao } = this.state;
    this.setState( {
      ordenacao: ( a, b ) => ( tipoOrdenacaoDuracao === 'ASC' ? a : b ).duracao - ( tipoOrdenacaoDuracao === 'ASC' ? b : a ).duracao,
      tipoOrdenacaoDuracao: tipoOrdenacaoDuracao === 'ASC' ? 'DESC' : 'ASC'
    } );
  }

  filtrarPorTermo = evt => {
    const termo = evt.target.value;
    this.episodiosApi.filtrarPorTermo( termo )
      .then( resultados => {
        this.setState({
          listaEpisodios: this.state.listaEpisodios.filter( e => resultados.some( r => r.episodioId === e.id ) )
        })
      } )
  }

  render() {
    const { listaEpisodios } = this.state;
    listaEpisodios.sort( this.state.ordenacao );

    return (
      <Fragment>
        <ListaEpisodiosUi className="home" listaEpisodios={ listaEpisodios } />
      </Fragment>
    )
  }

}