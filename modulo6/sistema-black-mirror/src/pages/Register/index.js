import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';

export default function Register() {
  const [nome, setNome] = useState('');
  const [senha, setSenha] = useState('');

  const history = useHistory();

  async function handleRegister(e) {
    e.preventDefault();

    const data = {
      nome,
      senha
    };
    try {
      const response = localStorage.setItem('user', data);
      alert(`Usuario ${response.data.nome} salvo com júbilo e regozijo`);
      history.push('/login');
    } catch (err) {
      alert('Erro no cadastro, tente novamente');
    }
  }

  return (
    <div className='register-container'>
      <div className='content'>
        <section>
          <h1>Cadastro</h1>
          <p>
            Faça seu cadastro.
          </p>
        </section>
        <form onSubmit={handleRegister}>
          <input
            placeholder='Nome da ONG'
            value={nome}
            onChange={e => setNome(e.target.value)}
          />
          <input
            type='password'
            placeholder='Senha'
            value={senha}
            onChange={e => setSenha(e.target.value)}
          />
          <button className='button' type='submit'>
            Cadastrar
          </button>
        </form>
      </div>
    </div>
  );
}
