import DetalhesEpisodios from './DetalhesEpisodios';
import Home from './Home';
import Ranking from './Ranking';
import Login from './Login';
import Register from './Register';

export { DetalhesEpisodios, Home, Ranking, Login, Register };