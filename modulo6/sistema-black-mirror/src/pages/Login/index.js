import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { BotaoUi } from '../../components';

export default function Login() {
  const [nome, setNome] = useState('');
  const [senha, setSenha] = useState('');
  const history = useHistory();

  async function handleLogin(e) {
    e.preventDefault();

    try {
      const response =  localStorage.getItem('user');
      if( nome === localStorage.getItem('nome', response.data.nome) 
      && senha === localStorage.getItem('senha', response.data.senha) ) {
        history.push('/');
      }
    } catch (err) {
      alert('Falha no Login');
    }
  }

  return (
    <div className='login-container'>
      <section className='form'>
        <form onSubmit={handleLogin}>
          <h1>Faça seu Login</h1>
          <input
            placeholder='Seu Nome'
            value={this.state.nome}
            onChange={e => setNome(e.target.value)}
          />

          <input
            placeholder='Sua senha'
            value={this.state.senha}
            onChange={e => setSenha(e.target.value)}
          />
          <button className='button' type='submit'>
            Entrar
          </button>

          <BotaoUi link='/register'>
            Não tenho cadastro
          </BotaoUi>
        </form>
      </section>
    </div>
  );
}

