import React, { Component, Fragment } from 'react';


import EpisodiosApi from '../../api/episodiosApi';
import { EpisodioUi, Estrelas } from '../../components';
import { Episodio } from '../../models';

export default class DetalhesEpisodios extends Component {
  constructor( props ) {
    super( props );
    this.episodioApi = new EpisodiosApi();
    this.mediaDasNotas = this.mediaDasNotas.bind( this );
    this.state = {
      detalhes: null,
    }
  }

  componentDidMount() {
    const episodioId = this.props.match.params.id;
    const requisicoes = [
      this.episodioApi.buscarEpisodio( episodioId ),
      this.episodioApi.buscarDetalhes( episodioId ),
      this.episodioApi.buscarNota( episodioId )
    ];

    Promise.all( requisicoes )
      .then( respostas => {
        const { id, nome, duracao, temporada, ordemEpisodio, thumbUrl, nota } = respostas[0];
        this.setState({
          episodio: new Episodio(id, nome, duracao, temporada, ordemEpisodio, thumbUrl, nota),
          detalhes: respostas[1],
          mediaNota: respostas[2]
        })
      })
  }

  mediaDasNotas ( mediaNota ) {
    const { length } = mediaNota;
    if( length === 0 ) {
      return 0;
    }
    let total = mediaNota.map( a => a.nota).reduce( function(acumula, nota){
      return acumula + nota;
    })
    return total / length;
  }

  registrarNota( { nota } ) {
    const { episodio } = this.state;
    episodio.avaliar( nota );
  }
  
  render() {
    const { episodio, detalhes, mediaNota } = this.state;
    return(
      <Fragment>
        <header className="App-header">
          { episodio && ( <EpisodioUi episodio={ episodio } /> )}
          {
            detalhes ?
            <Fragment>
              <p>{ detalhes.sinopse }</p>
              <span>{ new Date( detalhes.dataEstreia ).toLocaleDateString() }</span>
              <Estrelas id={ episodio.id } nota={ mediaNota ? this.mediaDasNotas(mediaNota) : 0 } precisao={0.5} />
            </Fragment> : null
          }
        </header>
      </Fragment>
    )
  }
} 